if (navigator.serviceWorker) {
  navigator.serviceWorker.register("/sw.js");
}

document.addEventListener("DOMContentLoaded", () => {
  hideAllTables();
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIds");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      data.forEach((item) => {
        const cellId = createCell(item.id);
        tbody.appendChild(createRow([cellId]));
        table.style.display = "table";
      });
    });
});

const createCell = (text) => {
  const cell = document.createElement("td");
  cell.classList.add(
    "px-6",
    "py-4",
    "font-medium",
    "text-gray-900",
    "whitespace-nowrap"
  );
  cell.textContent = text;
  return cell;
};

const createRow = (cells) => {
  const row = document.createElement("tr");
  row.classList.add("bg-white", "border-b");
  cells.forEach((cell) => row.appendChild(cell));
  return row;
};

const hideAllTables = () => {
  document.getElementById("tableIds").style.display = "none";
  document.getElementById("tableIdsTitles").style.display = "none";
  document.getElementById("tableIDsAndUserID").style.display = "none";
};
const getPendingTasksIds = () => {
  hideAllTables();
  document.getElementById("titleTable").textContent =
    "Lista de todos los pendientes (solo IDs)";
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIds");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      data.forEach((item) => {
        const cellId = createCell(item.id);
        table.appendChild(createRow([cellId]));
        table.style.display = "table";
        document.getElementById("titleTable").textContent =
          "Lista de todos los pendientes (solo IDs)";
      });
    });
};

const getPendingTasksIdsTitle = () => {
  hideAllTables();
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIdsTitles");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      data.forEach((item) => {
        const cellId = createCell(item.id);
        const cellTitle = createCell(item.title);
        tbody.appendChild(createRow([cellId, cellTitle]));
        table.style.display = "table";
        document.getElementById("titleTable").textContent =
          "Lista de todos los pendientes (IDs y Titles)";
      });
    });
};
const getPendingTasksUnresolvedIdsTitle = () => {
  hideAllTables();
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIdsTitles");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      const listPending = data.filter((item) => !item.completed);
      listPending.forEach((item) => {
        const cellId = createCell(item.id);
        const cellTitle = createCell(item.title);
        tbody.appendChild(createRow([cellId, cellTitle]));

        table.style.display = "table";
        document.getElementById("titleTable").textContent =
          "Lista de todos los pendientes sin resolver (IDs y Titles)";
      });
    });
};

const getPendingTasksResolvedIdsTitles = () => {
  hideAllTables();
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIdsTitles");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      const listCompleted = data.filter((item) => item.completed);
      listCompleted.forEach((item) => {
        const cellId = createCell(item.id);
        const cellTitle = createCell(item.title);
        tbody.appendChild(createRow([cellId, cellTitle]));

        table.style.display = "table";
        document.getElementById("titleTable").textContent =
          "Lista de todos los pendientes resultos (IDs y Titles)";
      });
    });
};

const getPendingTasksIdsUserIds = () => {
  hideAllTables();
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIDsAndUserID");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      data.forEach((item) => {
        const cellId = createCell(item.id);
        const cellUserId = createCell(item.userId);
        tbody.appendChild(createRow([cellId, cellUserId]));

        table.style.display = "table";
        document.getElementById("titleTable").textContent =
          "Lista de todos los pendientes (IDs y userID)";
      });
    });
};

const getPendingTasksResolvedIdsUserId = () => {
  hideAllTables();
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIDsAndUserID");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      const listCompleted = data.filter((item) => item.completed);
      listCompleted.forEach((item) => {
        const cellId = createCell(item.id);
        const cellUserId = createCell(item.userId);
        tbody.appendChild(createRow([cellId, cellUserId]));

        table.style.display = "table";
        document.getElementById("titleTable").textContent =
          "Lista de todos los pendientes resultos (IDs y userID)";
      });
    });
};
const getPendingTasksUnresolvedIdsUserID = () => {
  hideAllTables();
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((data) => {
      const table = document.getElementById("tableIDsAndUserID");
      const tbody = table.getElementsByTagName("tbody")[0];
      tbody.innerHTML = "";
      const listPending = data.filter((item) => !item.completed);
      listPending.forEach((item) => {
        const cellId = createCell(item.id);
        const cellUserId = createCell(item.userId);
        tbody.appendChild(createRow([cellId, cellUserId]));

        table.style.display = "table";
        document.getElementById("titleTable").textContent =
          "Lista de todos los pendientes sin resolver (IDs y UserID)";
      });
    });
};
