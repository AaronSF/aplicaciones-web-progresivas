// Evento fetch
self.addEventListener("fetch", (event) => {
  if (event.request.url.includes("todos")) {
    event.respondWith(
      fetch(event.request)
        .then((res) => res.json())
        .then((data) => {
          const newArray = data.map((element) => {
            // Creamos un nuevo objeto con el mismo contenido del original
            // y modificamos la propiedad 'id' añadiendo ":)"
            return { ...element, id: " :( " + element.id };
          });

          return new Response(JSON.stringify(newArray));
        })
    );
  }
  if (event.request.url.includes("perrito.jpg")) {
    event.respondWith(fetch("../img/nfl.png").then((res) => res));
  }
});
