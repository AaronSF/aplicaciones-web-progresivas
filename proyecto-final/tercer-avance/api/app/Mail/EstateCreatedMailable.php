<?php

namespace App\Mail;

use Google\Service\Dfareporting\Ad;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Address;
use App\Models\User;
use App\Models\Estate;

class EstateCreatedMailable extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $estate;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Estate $estate)
    {
        $this->user = $user;
        $this->estate = $estate;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('comunicacion@agrimarket.com', 'Agrimarket'),
            subject: 'Ligaste una granja a tu cuenta de Agrimarket!',
        );
    }

    /**
     * Get the message content definition.
     */
    public function build()
    {
        return $this->view('mailers.estate_created')
            ->with('user', $this->user)->with('estate', $this->estate);
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
