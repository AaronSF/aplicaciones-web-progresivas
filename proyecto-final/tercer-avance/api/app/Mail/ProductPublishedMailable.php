<?php

namespace App\Mail;

use Google\Service\Dfareporting\Ad;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Address;
use App\Models\User;
use App\Models\Estate;
use App\Models\Product;

class ProductPublishedMailable extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $product;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Product $product)
    {
        $this->user = $user;
        $this->product = $product;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('comunicacion@agrimarket.com', 'Agrimarket'),
            subject: '¡Tu producto ha sido publicado en Agrimarket!',
        );
    }

    /**
     * Get the message content definition.
     */
    public function build()
    {
        return $this->view('mailers.product_published')
            ->with('user', $this->user)->with('product', $this->product);
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
