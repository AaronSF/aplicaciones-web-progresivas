<?php

namespace App\Mail;

use Google\Service\Dfareporting\Ad;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Address;
use App\Models\User;
use App\Models\Order;

class OrderAcceptedMailable extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $order;

    /**
     * Create a new message instance.
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('comunicacion@agrimarket.com', 'Agrimarket'),
            subject: '¡Disfruta tu orden!',
        );
    }

    /**
     * Get the message content definition.
     */
    public function build()
    {
        return $this->view('mailers.order_accepted')
            ->with('user', $this->user)->with('order', $this->order);
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
