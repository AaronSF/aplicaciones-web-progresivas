<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Mail\OrderReceivedMailable;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderRequestedMailable;
use App\Mail\OrderAcceptedMailable;

class OrdersController extends Controller
{
    /**
     * Obtiene una lista de órdenes.
     *
     * Este endpoint devuelve una lista de órdenes de compra, filtradas según el rol del usuario autenticado.
     *
     * @authenticated
     * @group Órdenes
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "product_id": 1,
     *             "quantity": 10,
     *             "total": 100,
     *             "status": "Pendiente",
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         },
     *         {
     *             "id": 2,
     *             "product_id": 2,
     *             "quantity": 20,
     *             "total": 200,
     *             "status": "Pendiente",
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         }
     *     ]
     * }
     */

    public function index()
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('farmer')) {
            $orders = Order::where('farmer_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $orders]);
        } elseif ($user->hasRole('client')) {
            $orders = Order::where('client_id', $user->id)
                ->where('active', true)
                ->orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $orders]);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Muestra una orden específica.
     *
     * Este endpoint muestra detalles de una orden específica según el ID proporcionado.
     *
     * @authenticated
     * @group Órdenes
     * @urlParam id string required El ID de la orden.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "product_id": 1,
     *         "quantity": 10,
     *         "total": 100,
     *         "status": "Pendiente",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     */

    public function show(string $id)
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('farmer') || $user->hasRole('client')) {
            $order = $this->set_order($id);
            return response()->json(["data" => $order], 200);;
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Almacena una nueva orden.
     *
     * Este endpoint permite almacenar una nueva orden de compra en el sistema.
     *
     * @authenticated
     * @group Órdenes
     * @bodyParam product_id integer required El ID del producto.
     * @bodyParam quantity integer required La cantidad del producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "product_id": 1,
     *         "quantity": 10,
     *         "total": 100,
     *         "status": "Pendiente",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {"error": "Ocurrrio un error"}
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::guard('api')->user();
        $order = new Order($data);
        $product = Product::find($data["product_id"]);
        $order->client_id = $user->id;
        $order->farmer_id = $product->user_id;
        $order->unit_of_measurement_id = $product->unit_of_measurement->id;
        $order->status = "Pendiente";
        $order->total = ($data["quantity"] * $product->price_per_measure);
        if ($order->save()) {
            # Farmer email
            $order->farmer->sendPushNotification(
                "¡Recibiste una orden!",
                "Has recibido una nueva orden de {$order->product->product_type->name}. Revisa ahora y coordina la entrega con el cliente."
            );
            #Mail::to($user->email)->send(new OrderReceivedMailable($order->farmer, $order));
            # Client email
            #Mail::to($user->email)->send(new OrderRequestedMailable($order->client, $order));

            return response()->json(["data" => $order], 200);
        } else {
            return response()->json(["error" => "Ocurrrio un error"], 400);
        }
    }

    /**
     * Actualiza una orden existente.
     *
     * Este endpoint permite actualizar una orden existente en el sistema.
     *
     * @authenticated
     * @group Órdenes
     * @urlParam id string required El ID de la orden.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "product_id": 1,
     *         "quantity": 20,
     *         "total": 200,
     *         "status": "Pendiente",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     */

    public function update(Request $request, string $id)
    {
        $order = $this->set_order($id);
        $order->update($request->all());
        return response()->json(["data" => $order], 200);
    }

    /**
     * Actualiza el estado de una orden.
     *
     * Este endpoint permite actualizar el estado de una orden existente en el sistema.
     *
     * @authenticated
     * @group Órdenes
     * @urlParam id string required El ID de la orden.
     * @bodyParam status string required El nuevo estado de la orden.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "product_id": 1,
     *         "quantity": 10,
     *         "total": 100,
     *         "status": "Completado",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 500 {"error": "Descripción del error"}
     */

    public function update_status(Request $request, string $id)
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('farmer') || $user->hasRole('client')) {
            try {
                $data = $request->all();
                $order = Order::find($id);
                $product = $order->product;
                $order->status = $data["status"];
                if ($order->save()) {
                    if ($order->status == "Completado") {
                        $product->stock = (($product->stock) - ($order->quantity));
                        #Mail::to($user->email)->send(new OrderAcceptedMailable($order->client, $order));
                        $order->client->sendPushNotification(
                            "¡Tu orden fue aceptada!",
                            "La orden de {$order->product->product_type->name} que solicitaste ha sido aceptada por el agricultor. Prepárate para recibir tu pedido."
                        );
                        $product->save();
                    }
                    $order = $this->reduce_data($order);
                    return response()->json(["data" => $order], 200);
                }
            } catch (QueryException $e) {
                return response()->json(["error" => $e], 500);
            }
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Elimina una orden.
     *
     * Este endpoint permite eliminar una orden existente en el sistema.
     *
     * @authenticated
     * @group Órdenes
     * @urlParam id string required El ID de la orden.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "product_id": 1,
     *         "quantity": 10,
     *         "total": 100,
     *         "status": "Pendiente",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 500 {"error": "Descripción del error"}
     */

    public function destroy(string $id)
    {
        try {
            $order = Order::find($id);
            ($order->active) ? $order->active = false : $order->active = true;
            if ($order->save()) {
                return response()->json(["data" => $order], 200);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e], 500);
        }
    }

    private function set_order(string $id)
    {
        $order = Order::findOrFail($id);
        return $order;
    }

    private function reduce_data(object $order)
    {
        $data = [
            "id" => $order->id,
            "product_id" => $order->product_id,
            "quantity" => $order->quantity . " " . $order->unit_of_measurement->code,
            "total" => $order->total,
            "status" => $order->status,
            "created_at" => $order->created_at,
            "updated_at" => $order->updated_at
        ];
        return $data;
    }
}
