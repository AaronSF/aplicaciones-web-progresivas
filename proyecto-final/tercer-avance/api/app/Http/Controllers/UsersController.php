<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\UserData;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccountUpdatedMailable;

class UsersController extends Controller
{
    /**
     * Muestra una lista de todos los usuarios.
     * @group Usuarios
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": "ID del usuario",
     *             "first_name": "Nombre del usuario",
     *             "last_name": "Apellido del usuario",
     *             "email": "Correo electrónico del usuario",
     *             "role": "Rol del usuario",
     *             "created_at": "Fecha de creación del usuario",
     *             "updated_at": "Fecha de actualización del usuario"
     *         },
     *         ...
     *     ]
     * }
     */

    public function index()
    {
        $users = User::all();
        $users = $users->map(function ($user) {
            return $this->reduce_data($user);
        });
        return response()->json(["data" => $users], 200);
    }

    /**
     * Muestra el usuario especificado.
     * @group Usuarios
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID del usuario",
     *         "first_name": "Nombre del usuario",
     *         "last_name": "Apellido del usuario",
     *         "email": "Correo electrónico del usuario",
     *         "phone": "Número de teléfono del usuario",
     *         "street": "Calle del usuario",
     *         "ext_num": "Número exterior del usuario",
     *         "int_num": "Número interior del usuario",
     *         "suburb": "Colonia del usuario",
     *         "city": "Ciudad del usuario",
     *         "state": "Estado del usuario",
     *         "zip_code": "Código postal del usuario",
     *         "photo": "URL de la foto del usuario",
     *         "created_at": "Fecha de creación del usuario",
     *         "updated_at": "Fecha de actualización del usuario"
     *     }
     * }
     */

    public function show(string $id)
    {
        $user = $this->set_user($id);
        $user_data = UserData::where('user_id', $user->id)->first();
        if ($user_data) {
            $data = $this->set_complete_data($user, $user_data);
            return response()->json(["data" => $data], 200);
        }
        $data = $this->set_data($user, $user_data);
        return response()->json(["data" => $data], 200);
    }

    /**
     * Muestra el usuario autenticado.
     * @group Usuarios
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID del usuario",
     *         "first_name": "Nombre del usuario",
     *         "last_name": "Apellido del usuario",
     *         "email": "Correo electrónico del usuario",
     *         "phone": "Número de teléfono del usuario",
     *         "street": "Calle del usuario",
     *         "ext_num": "Número exterior del usuario",
     *         "int_num": "Número interior del usuario",
     *         "suburb": "Colonia del usuario",
     *         "city": "Ciudad del usuario",
     *         "state": "Estado del usuario",
     *         "zip_code": "Código postal del usuario",
     *         "photo": "URL de la foto del usuario",
     *         "created_at": "Fecha de creación del usuario",
     *         "updated_at": "Fecha de actualización del usuario"
     *     }
     * }
     */

    public function me()
    {
        $user = Auth::guard('api')->user();
        $u_data = UserData::where("user_id", $user->id)->first();
        if ($u_data) {
            $data = $this->set_complete_data($user, $u_data);
            return response()->json(["data" => $data]);
        }
        return response()->json(["data" => [
            "id" => $user->id,
            "first_name" => $user->first_name,
            "last_name" => $user->last_name,
            "email" => $user->email
        ]]);
    }

    /**
     * Almacena la push ID del usuario autenticado.
     * @group Usuarios
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     * @response {"data": "Push ID actualizada"}
     * @response 400 {"error": "No se recibió la push ID"}
     */

    public function receivePushId(Request $request)
    {
        # Obtener la push ID del request
        $data = $request->all();
        # Obtener el usuario autenticado
        $user = Auth::guard('api')->user();
        # Buscar al usuario con el ID
        $user = User::find($user->id);
        # Actualizar la push ID
        if ($data['push_id'] != null) {
            $user->push_id = $data['push_id'];
            $user->save();
            return response()->json(["data" => "Push ID actualizada"]);
        }
        return response()->json(["error" => "No se recibió la push ID"], 400);
    }

    /**
     * Actualiza los datos del usuario autenticado.
     * @group Usuarios
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID del usuario",
     *         "first_name": "Nombre del usuario",
     *         "last_name": "Apellido del usuario",
     *         "email": "Correo electrónico del usuario",
     *         "phone": "Número de teléfono del usuario",
     *         "street": "Calle del usuario",
     *         "ext_num": "Número exterior del usuario",
     *         "int_num": "Número interior del usuario",
     *         "suburb": "Colonia del usuario",
     *         "city": "Ciudad del usuario",
     *         "state": "Estado del usuario",
     *         "zip_code": "Código postal del usuario",
     *         "photo": "URL de la foto del usuario",
     *         "created_at": "Fecha de creación del usuario",
     *         "updated_at": "Fecha de actualización del usuario"
     *     }
     * }
     */

    public function update_me(Request $request)
    {
        $data = $request->all();
        $user = Auth::guard('api')->user();
        $user = User::find($user->id);
        $user->update($data);
        # AQUI DEBO DE AGREGAR EL MAILER WUAAA
        #Mail::to($user->email)->send(new AccountUpdatedMailable($user));
        $u_data = UserData::where("user_id", $user->id)->first();
        // return response()->json(["data" => $this->set_data($user, $u_data)]);
        if ($u_data) {
            $url = $u_data->photo;
            $u_data->update($data);
            $u_data->photo = $url;
            $u_data->save();
            if ($request->file('photo')) {
                $url = $this->upload_photo($request);
                $u_data->photo = $url->original["url"];
                $u_data->save();
            }
            $u_data = $this->set_complete_data($user, $u_data);
            return response()->json(["data" => $u_data]);
        } else {
            if (isset($data["phone"]) && isset($data["street"])) {
                $u_data = new UserData($data);
                if ($request->file('photo')) {
                    $url = $this->upload_photo($request);
                    $u_data->photo = $url->original["url"];
                }
                $u_data->user_id = $user->id;
                $u_data->save();
                $u_data = $this->set_complete_data($user, $u_data);
                return response()->json(["data" => $u_data]);
            }
        }

        return response()->json(["data" => [
            "id" => $user->id,
            "first_name" => $user->first_name,
            "last_name" => $user->last_name,
            "email" => $user->email
        ]]);
    }

    /**
     * Busca usuarios por nombre, apellido o correo electrónico.
     * @group Usuarios
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": "ID del usuario",
     *             "first_name": "Nombre del usuario",
     *             "last_name": "Apellido del usuario",
     *             "email": "Correo electrónico del usuario",
     *             "role": "Rol del usuario",
     *             "created_at": "Fecha de creación del usuario",
     *             "updated_at": "Fecha de actualización del usuario"
     *         },
     *         ...
     *     ]
     * }
     * @response 400 {"error": "No se encontró un nombre, apellido o e-mail"}
     */

    public function find_user(Request $request)
    {
        if ($request->has('name')) {
            $name = $request->input('name');
            $users = User::whereRaw("LOWER(first_name) LIKE LOWER(?) OR LOWER(last_name) LIKE LOWER(?) OR email LIKE ?", ["%{$name}%", "%{$name}%", "%{$name}%"])
                ->orderBy('first_name')
                ->get();
            // Retornar los resultados en formato JSON
            return response()->json(['data' => $users]);
        }
        return response()->json(['error' => "No se encontró un nombre, apellido o e-mail"], 400);
    }


    /**
     * Elimina el usuario especificado.
     * @group Usuarios
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {"data": "Usuario eliminado"}
     */

    public function destroy(string $id)
    {
        $user = $this->set_user($id);
        $user->delete();
        return response()->json(["data" => "Usuario eliminado"], 200);
    }

    private function set_user(string $id)
    {
        $user = User::findOrFail($id);
        return $user;
    }

    private function set_complete_data(object $user, object $user_data)
    {
        $data = [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            "phone" => $user_data->phone,
            "street" => $user_data->street,
            "ext_num" => $user_data->ext_num,
            "int_num" => $user_data->int_num,
            "suburb" => $user_data->suburb,
            "city" => $user_data->city,
            "state" => $user_data->state,
            "zip_code" => $user_data->zip_code,
            "photo" => $user_data->photo,
            "created_at" => $user->created_at,
            "updated_at" => $user->updated_at
        ];
        return $data;
    }


    private function set_data(object $user)
    {
        $data = [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'push_id' => $user->push_id,
            "created_at" => $user->created_at,
            "updated_at" => $user->updated_at
        ];
        return $data;
    }

    private function reduce_data(object $user)
    {
        $data = [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            "role" => $user->getRoleNames()->first(),
            "created_at" => $user->created_at,
            "updated_at" => $user->updated_at
        ];
        return $data;
    }

    private function upload_photo(Request $request)
    {
        $response = cloudinary()->upload($request->file('photo')->getRealPath())->getSecurePath();
        return response()->json(['url' => $response]);
    }
}
