<?php

namespace App\Http\Controllers;

use App\Mail\ProductPublishedMailable;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Models\Photo;
use Illuminate\Support\Facades\Mail;

class ProductsController extends Controller
{
    /**
     * Muestra una lista de productos.
     *
     * Este endpoint devuelve una lista de productos disponibles en el sistema.
     *
     * @authenticated
     * @group Productos
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "user_id": "Nombre del usuario",
     *             "product": "Nombre del producto",
     *             "price": 10.99,
     *             "measure": "Unidad de medida",
     *             "minimum_sale": 5,
     *             "cutoff_date": "Fecha de corte",
     *             "photos": [
     *                 {"photo": "URL de la foto 1"},
     *                 {"photo": "URL de la foto 2"}
     *             ],
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         },
     *         {
     *             ...
     *         }
     *     ]
     * }
     * @response 400 {"error": "Tu usuario no cuenta con un rol indicado"}
     */

    public function index()
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('farmer')) {
            $products = Product::where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->get();
            $products = $products->map(function ($pt) {
                return $this->reduce_data($pt);
            });
            return response()->json(["data" => $products]);
        } elseif ($user->hasRole('client')) {
            $products = Product::where('active', true)
                ->orderBy('created_at', 'desc')
                ->get();
            $products = $products->map(function ($pt) {
                return $this->reduce_data($pt);
            });
            return response()->json(["data" => $products]);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);;
    }

    /**
     * Muestra un producto específico.
     *
     * Este endpoint devuelve información detallada sobre un producto específico.
     *
     * @authenticated
     * @group Productos
     * @urlParam id string required El ID del producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "user_id": "Nombre del usuario",
     *         "product": "Nombre del producto",
     *         "description": "Descripción del producto",
     *         "price": 10.99,
     *         "measure": "Unidad de medida",
     *         "stock": 100,
     *         "minimum_sale": 5,
     *         "cutoff_date": "Fecha de corte",
     *         "photos": [
     *             {"photo": "URL de la foto 1"},
     *             {"photo": "URL de la foto 2"}
     *         ],
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {"error": "Tu usuario no cuenta con un rol indicado"}
     */

    public function show(string $id)
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('farmer') || $user->hasRole('client')) {
            $product = $this->set_product($id);
            return response()->json(["data" => $this->set_complete_data($product)], 200);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Almacena un nuevo producto.
     *
     * Este endpoint permite a un usuario almacenar un nuevo producto en el sistema.
     *
     * @authenticated
     * @group Productos
     * @bodyParam product_type_id int required El ID del tipo de producto.
     * @bodyParam description string Descripción del producto.
     * @bodyParam price_per_measure float required Precio por unidad de medida.
     * @bodyParam unit_of_measurement_id int required El ID de la unidad de medida.
     * @bodyParam minimum_sale int Cantidad mínima de venta.
     * @bodyParam cutoff_date date Fecha de corte.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "user_id": "Nombre del usuario",
     *         "product": "Nombre del producto",
     *         "price": 10.99,
     *         "measure": "Unidad de medida",
     *         "minimum_sale": 5,
     *         "cutoff_date": "Fecha de corte",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {"error": "Ocurrrio un error"}
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::guard('api')->user();
        $product = new Product($data);
        $product->user_id = $user->id;
        if ($product->save()) {
            #Mail::to($user->email)->send(new ProductPublishedMailable($user, $product));
            return response()->json(["data" => $product], 200);
        } else {
            return response()->json(["error" => "Ocurrrio un error"], 400);
        }
    }


    /**
     * Actualiza un producto existente.
     *
     * Este endpoint permite actualizar la información de un producto existente en el sistema.
     *
     * @authenticated
     * @group Productos
     * @urlParam id string required El ID del producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "user_id": "Nombre del usuario",
     *         "product": "Nombre del producto",
     *         "price": 10.99,
     *         "measure": "Unidad de medida",
     *         "minimum_sale": 5,
     *         "cutoff_date": "Fecha de corte",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {"error": "Tu usuario no cuenta con un rol indicado"}
     */

    public function update(Request $request, string $id)
    {
        $product = $this->set_product($id);
        $product->update($request->all());
        return response()->json(["data" => $product], 200);
    }

    /**
     * Elimina un producto existente.
     *
     * Este endpoint permite eliminar un producto existente en el sistema.
     *
     * @authenticated
     * @group Productos
     * @urlParam id string required El ID del producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "user_id": "Nombre del usuario",
     *         "product": "Nombre del producto",
     *         "price": 10.99,
     *         "measure": "Unidad de medida",
     *         "minimum_sale": 5,
     *         "cutoff_date": "Fecha de corte",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 500 {"error": "Descripción del error"}
     */

    public function destroy(string $id)
    {
        try {
            $product = Product::find($id);
            ($product->active) ? $product->active = false : $product->active = true;
            if ($product->save()) {
                return response()->json(["data" => $product], 200);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e], 500);
        }
    }

    /**
     * Establece el producto especificado.
     *
     * Este método busca y devuelve el producto correspondiente al ID proporcionado.
     *
     * @param string $id El ID del producto.
     * @return \App\Models\Product El producto correspondiente al ID proporcionado.
     */

    private function set_product(string $id)
    {
        $product = Product::findOrFail($id);
        return $product;
    }

    /**
     * Reduce los datos del producto especificado.
     *
     * Este método reduce los datos del producto especificado para su presentación en la respuesta.
     *
     * @param object $pt El producto.
     * @return array Los datos reducidos del producto.
     */

    private function reduce_data(object $pt)
    {
        $photos = Photo::where("product_id", $pt->id)->get();
        $photos = $photos->map(function ($pt) {
            return ["photo" => $pt->photo];
        });
        $data = [
            "id" => $pt->id,
            "user_id" => $pt->user->first_name . " " . $pt->user->last_name,
            "product" => $pt->product_type->name,
            "price" => $pt->price_per_measure,
            "measure" => $pt->unit_of_measurement->name,
            "minimum_sale" => $pt->minimum_sale,
            "cutoff_date" => $pt->cutoff_date,
            "photos" => $photos,
            "created_at" => $pt->created_at,
            "updated_at" => $pt->updated_at
        ];
        return $data;
    }

    /**
     * Establece los datos completos del producto especificado.
     *
     * Este método establece los datos completos del producto especificado para su presentación en la respuesta.
     *
     * @param object $pt El producto.
     * @return array Los datos completos del producto.
     */

    private function set_complete_data(object $pt)
    {
        $photos = Photo::where("product_id", $pt->id)->get();
        $photos = $photos->map(function ($pt) {
            return ["photo" => $pt->photo];
        });
        $data = [
            "id" => $pt->id,
            "user_id" => $pt->user->first_name . " " . $pt->user->last_name,
            "product" => $pt->product_type->name,
            "description" => $pt->description,
            "price" => $pt->price_per_measure,
            "measure" => $pt->unit_of_measurement->name,
            "stock" => $pt->stock,
            "minimum_sale" => $pt->minimum_sale,
            "cutoff_date" => $pt->cutoff_date,
            "photos" => $photos,
            "created_at" => $pt->created_at,
            "updated_at" => $pt->updated_at
        ];
        return $data;
    }
}
