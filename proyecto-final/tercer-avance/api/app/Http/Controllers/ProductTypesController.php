<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\ProductType;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class ProductTypesController extends Controller
{
    /**
     * Muestra una lista de tipos de productos.
     *
     * Este endpoint devuelve una lista de tipos de productos disponibles en el sistema.
     *
     * @authenticated
     * @group Tipos de Productos
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "name": "Nombre del tipo de producto",
     *             "category": "Nombre de la categoría",
     *             "active": true,
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         },
     *         {
     *             ...
     *         }
     *     ]
     * }
     * @response 400 {"error": "Tu usuario no cuenta con un rol indicado"}
     */

    public function index()
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('admin')) {
            $product_types = ProductType::orderBy('created_at', 'desc')
                ->get();
            $product_types = $product_types->map(function ($pt) {
                return $this->reduce_data($pt);
            });
            return response()->json(["data" => $product_types]);
        } elseif ($user->hasRole('farmer')) {
            $product_types = ProductType::where('active', true)
                ->orderBy('created_at', 'desc')
                ->get();
            $product_types = $product_types->map(function ($pt) {
                return $this->reduce_data($pt);
            });
            return response()->json(["data" => $product_types]);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Busca tipos de productos por nombre.
     *
     * Este endpoint permite buscar tipos de productos por su nombre.
     *
     * @authenticated
     * @group Tipos de Productos
     * @queryParam name string required El nombre del tipo de producto a buscar.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "name": "Nombre del tipo de producto",
     *             "category": "Nombre de la categoría",
     *             "active": true,
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         },
     *         {
     *             ...
     *         }
     *     ]
     * }
     * @response 400 {"error": "No se encontró ningún registro"}
     */

    public function find_product_type(Request $request)
    {
        if ($request->has('name')) {
            $name = $request->input('name');
            $product_types = ProductType::whereRaw("LOWER(name) LIKE LOWER(?)", ["%{$name}%"])
                ->orderBy('name')
                ->get();
            $product_types = $product_types->map(function ($pt) {
                return $this->reduce_data($pt);
            });
            return response()->json(['data' => $product_types]);
        }
        return response()->json(['error' => "No se encontró ningún registro"], 400);
    }


    /**
     * Almacena un nuevo tipo de producto.
     *
     * Este endpoint permite a un usuario almacenar un nuevo tipo de producto en el sistema.
     *
     * @authenticated
     * @group Tipos de Productos
     * @bodyParam name string required El nombre del tipo de producto.
     * @bodyParam category_id int required El ID de la categoría del tipo de producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre del tipo de producto",
     *         "category": "Nombre de la categoría",
     *         "active": true,
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {"error": "Ocurrrio un error"}
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $product_type = new ProductType($data);
        if ($product_type->save()) {
            return response()->json(["data" => $product_type], 200);
        } else {
            return response()->json(["error" => "Ocurrrio un error"], 400);
        }
    }

    /**
     * Muestra un tipo de producto específico.
     *
     * Este endpoint devuelve información detallada sobre un tipo de producto específico.
     *
     * @authenticated
     * @group Tipos de Productos
     * @urlParam id string required El ID del tipo de producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre del tipo de producto",
     *         "category": "Nombre de la categoría",
     *         "active": true,
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     */

    public function show(string $id)
    {
        $product_type =  $this->set_product_type($id);
        return response()->json(["data" => $product_type], 200);
    }


    /**
     * Actualiza un tipo de producto existente.
     *
     * Este endpoint permite actualizar la información de un tipo de producto existente en el sistema.
     *
     * @authenticated
     * @group Tipos de Productos
     * @urlParam id string required El ID del tipo de producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre del tipo de producto",
     *         "category": "Nombre de la categoría",
     *         "active": true,
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     */

    public function update(Request $request, string $id)
    {
        $product_type =  $this->set_product_type($id);
        $product_type->update($request->all());
        return response()->json(["data" => $product_type], 200);
    }

    /**
     * Elimina un tipo de producto existente.
     *
     * Este endpoint permite eliminar un tipo de producto existente en el sistema.
     *
     * @authenticated
     * @group Tipos de Productos
     * @urlParam id string required El ID del tipo de producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre del tipo de producto",
     *         "category": "Nombre de la categoría",
     *         "active": true,
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 500 {"error": "Descripción del error"}
     */

    public function destroy(string $id)
    {
        try {
            $product_type = ProductType::find($id);
            ($product_type->active) ? $product_type->active = false : $product_type->active = true;
            if ($product_type->save()) {
                return response()->json(["data" => $product_type], 200);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e], 500);
        }
    }

    /**
     * Establece el tipo de producto utilizando su ID.
     *
     * Este método busca y establece el tipo de producto utilizando su ID.
     *
     * @param string $id El ID del tipo de producto.
     * @return \App\Models\ProductType El tipo de producto encontrado.
     */

    private function set_product_type(string $id)
    {
        $product_type = ProductType::findOrFail($id);
        return $product_type;
    }

    /**
     * Reduce los datos del tipo de producto para su presentación.
     *
     * Este método reduce los datos del tipo de producto para su presentación en la respuesta.
     *
     * @param object $pt El tipo de producto.
     * @return array Los datos reducidos del tipo de producto.
     */

    private function reduce_data(object $pt)
    {
        $data = [
            "id" => $pt->id,
            "name" => $pt->name,
            "category" => $pt->category->name,
            "active" => $pt->active,
            "created_at" => $pt->created_at,
            "updated_at" => $pt->updated_at
        ];
        return $data;
    }
}
