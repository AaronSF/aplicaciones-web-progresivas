<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Models\UserData;

class FarmersController extends Controller
{
    /**
     * Obtiene estadísticas del panel de control del agricultor.
     *
     * Este endpoint devuelve estadísticas del panel de control del agricultor, como ganancias, total de órdenes completadas y total de productos.
     *
     * @authenticated
     * @group Agricultores
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "completed_orders": 1000,
     *         "total_orders": 50,
     *         "products": 20
     *     }
     * }
     */

    public function dashboard()
    {
        $user = Auth::guard('api')->user();
        //Ganancias
        $completed_orders = Order::where("farmer_id", $user->id)->where("status", 'Completado')->get();
        $sales = 0;
        foreach ($completed_orders as $item) {
            $sales += $item['total'];
        }
        //Ordenes completadas
        $total_orders = Order::where("farmer_id", $user->id)->get();
        $products = Product::where("user_id", $user->id)->get();
        return response()->json([
            "data" => [
                "completed_orders" => $sales,
                "total_orders" => count($total_orders),
                "products" => count($products)
            ]
        ]);
    }

    /**
     * Obtiene los productos más vendidos del agricultor.
     *
     * Este endpoint devuelve los productos más vendidos del agricultor.
     *
     * @authenticated
     * @group Agricultores
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "product_name": "Producto 1",
     *             "quantity": 50,
     *             "total_sales": 1000
     *         },
     *         {
     *             "id": 2,
     *             "product_name": "Producto 2",
     *             "quantity": 30,
     *             "total_sales": 800
     *         }
     *     ]
     * }
     */

    public function top_sales()
    {
        $user = Auth::guard('api')->user();
        $products = Order::join('products', 'orders.product_id', '=', 'products.id')
            ->where('products.user_id', $user->id)
            ->select('orders.*')
            ->groupBy('orders.id')
            ->orderByRaw('COUNT(*) DESC')
            ->take(3)
            ->get();
        return response()->json(["data" => $products]);
    }

    /**
     * Obtiene las últimas órdenes pendientes del agricultor.
     *
     * Este endpoint devuelve las últimas órdenes pendientes del agricultor.
     *
     * @authenticated
     * @group Agricultores
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "order_number": "ORD-001",
     *             "total": 100,
     *             "status": "Pendiente",
     *             "created_at": "Fecha de creación"
     *         },
     *         {
     *             "id": 2,
     *             "order_number": "ORD-002",
     *             "total": 150,
     *             "status": "Pendiente",
     *             "created_at": "Fecha de creación"
     *         }
     *     ]
     * }
     */

    public function last_orders()
    {
        $user = Auth::guard('api')->user();
        $orders = Order::where('status', 'pendiente')
            ->where("farmer_id", $user->id)
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json(["data" => $orders]);
    }
}
