<?php

namespace App\Http\Controllers;

use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\JsonResponse;
use Laravel\Socialite\Contracts\User as SocialiteUser;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    /**
     * Redirecciona al proveedor de autenticación social para la autorización.
     *
     * Este endpoint redirecciona al usuario al proveedor de autenticación social (Google)
     * para que pueda autorizar la aplicación.
     * @group Autenticación
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "url": "URL de redirección para la autorización"
     * }
     */

    public function redirectToAuth(): JsonResponse
    {
        return response()->json([
            'url' => Socialite::driver('google')
                ->stateless()
                ->redirect()
                ->getTargetUrl(),
        ]);
    }


    /**
     * Maneja el callback de autenticación social.
     *
     * Este endpoint maneja la respuesta del proveedor de autenticación social después de la autorización.
     * @group Autenticación
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "user": {
     *         "id": "ID del usuario",
     *         "email": "Correo electrónico del usuario",
     *         "email_verified_at": "Fecha de verificación del correo electrónico",
     *         "name": "Nombre del usuario",
     *         "password": "Contraseña del usuario",
     *         "google_id": "ID de Google del usuario",
     *         "avatar": "URL del avatar del usuario",
     *         "created_at": "Fecha de creación del usuario",
     *         "updated_at": "Fecha de actualización del usuario"
     *     },
     *     "access_token": "Token de acceso del usuario",
     *     "token_type": "Bearer"
     * }
     * @response 422 {"error": "Invalid credentials provided."}
     */

    public function handleAuthCallback(): JsonResponse
    {
        try {
            /** @var SocialiteUser $socialiteUser */
            $socialiteUser = Socialite::driver('google')->stateless()->user();
        } catch (ClientException $e) {
            return response()->json(['error' => 'Invalid credentials provided.'], 422);
        }

        /** @var User $user */
        $user = User::query()
            ->firstOrCreate(
                [
                    'email' => $socialiteUser->getEmail(),
                ],
                [
                    'email_verified_at' => now(),
                    'name' => $socialiteUser->getName(),
                    'password' => $socialiteUser->getId(),
                    'google_id' => $socialiteUser->getId(),
                    'avatar' => $socialiteUser->getAvatar(),
                ]
            );

        return response()->json([
            'user' => $user,
            'access_token' => $user->createToken('google-token')->plainTextToken,
            'token_type' => 'Bearer',
        ]);
    }
}
