<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhotosController extends Controller
{
    /**
     * Almacena una nueva foto para un producto.
     *
     * Este endpoint permite almacenar una nueva foto para un producto específico.
     *
     * @authenticated
     * @group Fotos
     * @urlParam id string required El ID del producto.
     * @bodyParam photo file required La foto del producto a subir.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "product_id": 1,
     *         "photo": "URL de la foto",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {"error": "Ocurrrio un error"}
     */

    public function store(Request $request, string $id)
    {
        if ($this->store_validation($id)) {
            $photo = new Photo();
            $url = $this->upload_photo($request);
            $photo->product_id = $id;
            $photo->photo = $url->original["url"];
            if ($photo->save()) {
                return response()->json(["data" => $photo], 200);
            } else {
                return response()->json(["error" => "Ocurrrio un error"], 400);
            }
        }
    }

    /**
     * Elimina una foto de un producto.
     *
     * Este endpoint permite eliminar una foto de un producto existente en el sistema.
     *
     * @authenticated
     * @group Fotos
     * @urlParam id string required El ID de la foto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": "Foto eliminada"
     * }
     */

    public function destroy(string $id)
    {
        $photo =  $this->set_photo($id);
        $photo->delete();
        return response()->json(["data" => "Foto eliminada"], 200);
    }

    /**
     * Establece la foto especificada.
     *
     * Este método busca y devuelve la foto correspondiente al ID proporcionado.
     *
     * @param string $id El ID de la foto.
     * @return \App\Models\Photo La foto correspondiente al ID proporcionado.
     */

    private function set_photo(string $id)
    {
        $photo = Photo::findOrFail($id);
        return $photo;
    }

    /**
     * Sube la foto proporcionada a Cloudinary.
     *
     * Este método carga la foto proporcionada a Cloudinary y devuelve la URL de la foto cargada.
     *
     * @param \Illuminate\Http\Request $request La solicitud HTTP con la foto a cargar.
     * @return \Illuminate\Http\JsonResponse La URL de la foto cargada.
     */

    private function upload_photo(Request $request)
    {
        $response = cloudinary()->upload($request->file('photo')->getRealPath())->getSecurePath();
        return response()->json(['url' => $response]);
    }

    /**
     * Valida si se puede almacenar una nueva foto para el producto especificado.
     *
     * Este método verifica si el número de fotos almacenadas para el producto especificado es menor que 5.
     *
     * @param string $id El ID del producto.
     * @return bool True si se puede almacenar una nueva foto, de lo contrario, False.
     */

    private function store_validation(string $id)
    {
        $product = Product::find($id);
        $stored_photos = count($product->photos);
        $stored_photos < 5 ? $value = true : $value = false;
        return $value;
    }
}
