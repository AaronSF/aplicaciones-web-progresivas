<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UnitOfMeasurement;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class UnitOfMeasurementsController extends Controller
{
    /**
     * Muestra una lista de todas las unidades de medida.
     * @group Unidades de medida
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": "ID de la unidad de medida",
     *             "name": "Nombre de la unidad de medida",
     *             "code": "Código de la unidad de medida",
     *             "active": "Estado de la unidad de medida (activo o inactivo)",
     *             "created_at": "Fecha de creación de la unidad de medida",
     *             "updated_at": "Fecha de actualización de la unidad de medida"
     *         },
     *         ...
     *     ]
     * }
     */

    public function index()
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('admin')) {
            $unit = UnitOfMeasurement::orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $unit]);
        } elseif ($user->hasRole('farmer')) {
            $unit = UnitOfMeasurement::where('active', true)
                ->orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $unit]);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Almacena una nueva unidad de medida.
     * @group Unidades de medida
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID de la unidad de medida",
     *         "name": "Nombre de la unidad de medida",
     *         "code": "Código de la unidad de medida",
     *         "active": "Estado de la unidad de medida (activo o inactivo)",
     *         "created_at": "Fecha de creación de la unidad de medida",
     *         "updated_at": "Fecha de actualización de la unidad de medida"
     *     }
     * }
     * @response 400 {"error": "Ocurrió un error al almacenar la unidad de medida."}
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $unit = new UnitOfMeasurement($data);
        if ($unit->save()) {
            return response()->json(["data" => $unit], 200);
        } else {
            return response()->json(["error" => "Ocurrrio un error"], 400);
        }
    }

    /**
     * Muestra la unidad de medida especificada.
     * @group Unidades de medida
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID de la unidad de medida",
     *         "name": "Nombre de la unidad de medida",
     *         "code": "Código de la unidad de medida",
     *         "active": "Estado de la unidad de medida (activo o inactivo)",
     *         "created_at": "Fecha de creación de la unidad de medida",
     *         "updated_at": "Fecha de actualización de la unidad de medida"
     *     }
     * }
     */

    public function show(string $id)
    {
        $unit =  $this->set_category($id);
        return response()->json(["data" => $unit], 200);
    }

    /**
     * Actualiza la unidad de medida especificada en el almacenamiento.
     * @group Unidades de medida
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID de la unidad de medida",
     *         "name": "Nombre de la unidad de medida",
     *         "code": "Código de la unidad de medida",
     *         "active": "Estado de la unidad de medida (activo o inactivo)",
     *         "created_at": "Fecha de creación de la unidad de medida",
     *         "updated_at": "Fecha de actualización de la unidad de medida"
     *     }
     * }
     */
    public function update(Request $request, string $id)
    {
        $unit =  $this->set_category($id);
        $unit->update($request->all());
        return response()->json(["data" => $unit], 200);
    }

    /**
     * Elimina la unidad de medida especificada del almacenamiento.
     * @group Unidades de medida
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {"data": "Unidad de medida eliminada"}
     */

    public function destroy(string $id)
    {
        try {
            $unit = UnitOfMeasurement::find($id);
            ($unit->active) ? $unit->active = false : $unit->active = true;
            if ($unit->save()) {
                return response()->json(["data" => $unit], 200);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e], 500);
        }
    }

    /**
     * Establece la unidad de medida especificada.
     *
     * @param  string  $id
     * @return \App\Models\UnitOfMeasurement
     */

    private function set_category(string $id)
    {
        $unit = UnitOfMeasurement::findOrFail($id);
        return $unit;
    }
}
