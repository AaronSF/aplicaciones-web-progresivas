<?php

namespace App\Http\Controllers;

use App\Mail\AccountCreatedMailable;
use App\Mail\AccountUpdatedMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User; // Asegúrate de importar el modelo User
use Spatie\Permission\Models\Role;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Mail;

class AuthenticationController extends Controller
{
    /**
     * Registra un nuevo usuario.
     * 
     * Este endpoint permite registrar un nuevo usuario en el sistema.
     * 
     * @group Autenticación
     * @bodyParam first_name string required El nombre del usuario.
     * @bodyParam last_name string required El apellido del usuario.
     * @bodyParam email string required El correo electrónico del usuario.
     * @bodyParam password string required La contraseña del usuario.
     * @bodyParam google_id string El ID de Google del usuario (opcional).
     * @bodyParam type string required El tipo de usuario (2 para agricultor, cualquier otro valor para cliente).
     * 
     * @response {
     *     "user": {
     *         "id": 1,
     *         "first_name": "John",
     *         "last_name": "Doe",
     *         "email": "john@example.com",
     *         "role": "client",
     *         "access_token": "..."
     *     }
     * }
     * 
     * @response 500 {"error": "..." }
     */

    public function register(Request $request)
    {
        try {
            $data = $request->all();
            $user = new User($data);
            if ($user->save()) {
                ($request->input('type') == "2") ? $user->assignRole('farmer') : $user->assignRole('client');
                # Send email to user
                #Mail::to($user->email)->send(new AccountCreatedMailable($user));
                //login
                $credentials = $request->only('email', 'password');
                Auth::attempt($credentials);
                $user = Auth::user();

                if ($user instanceof \App\Models\User) {
                    $accessToken = $user->createToken('token')->accessToken;
                    return response()->json([
                        "user" => $this->set_data($user, $accessToken)
                    ]);
                }
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e]);
        }
    }

    /**
     * Inicia sesión de usuario.
     * 
     * Este endpoint permite a un usuario iniciar sesión en el sistema.
     * 
     * @group Autenticación
     * @bodyParam email string required El correo electrónico del usuario.
     * @bodyParam password string required La contraseña del usuario.
     * 
     * @response {
     *     "user": {
     *         "id": 1,
     *         "first_name": "John",
     *         "last_name": "Doe",
     *         "email": "john@example.com",
     *         "role": "client",
     *         "access_token": "..."
     *     }
     * }
     * 
     * @response 401 {"message": "Usuario y/o contraseña inválidos." }
     * @response 401 {"message": "Error de autenticación." }
     * @response 500 {"error": "..." }
     */

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return response()->json([
                "message" => "Usuario y/o contraseña inválidos.",
            ], 401);
        }

        $user = Auth::user();

        try {
            if ($user instanceof \App\Models\User) {
                $accessToken = $user->createToken('token')->accessToken;
                return response()->json([
                    "user" => $this->set_data($user, $accessToken)
                ]);
            } else {
                return response()->json([
                    "message" => "Error de autenticación.",
                ], 401);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e]);
        }
    }

    public function set_data(object $user, string $token)
    {
        $data = [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            "role" => $user->getRoleNames()->first(),
            "access_token" => $token,
        ];
        return $data;
    }
}
