<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Mail\EstateCreatedMailable;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Mail\EstateUpdatedMailable;

class EstatesController extends Controller
{
    /**
     * Obtiene todas las propiedades inmobiliarias del usuario autenticado.
     *
     * Este endpoint devuelve todas las propiedades inmobiliarias activas del usuario autenticado.
     *
     * @authenticated
     * @group Propiedades Inmobiliarias
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "title": "Título de la propiedad",
     *             "description": "Descripción de la propiedad",
     *             "photo": "URL de la foto",
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         },
     *         {
     *             "id": 2,
     *             "title": "Otra propiedad",
     *             "description": "Otra descripción",
     *             "photo": "URL de la foto",
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         }
     *     ]
     * }
     */

    public function index()
    {
        $user = Auth::guard('api')->user();
        $estates = Estate::where('user_id', $user->id)
            ->where('active', true)
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json(["data" => $estates]);
    }

    /**
     * Almacena una nueva propiedad inmobiliaria.
     *
     * Este endpoint permite almacenar una nueva propiedad inmobiliaria en el sistema.
     *
     * @authenticated
     * @group Propiedades Inmobiliarias
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "title": "Título de la propiedad",
     *         "description": "Descripción de la propiedad",
     *         "photo": "URL de la foto",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {
     *     "error": "Ocurrió un error"
     * }
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::guard('api')->user();
        $user = User::find($user->id);
        $estate = new Estate($data);
        $estate->user_id = $user->id;
        if ($request->file('photo')) {
            $url = $this->upload_photo($request);
            $estate->photo = $url->original["url"];
        }
        if ($estate->save()) {
            #Mail::to($user->email)->send(new EstateCreatedMailable($user, $estate));
            return response()->json(["data" => $estate], 200);
        } else {
            return response()->json(["error" => "Ocurrrio un error"], 400);
        }
    }

    /**
     * Muestra una propiedad inmobiliaria específica.
     *
     * Este endpoint muestra los detalles de una propiedad inmobiliaria específica.
     *
     * @authenticated
     * @group Propiedades Inmobiliarias
     * @param string $id El ID de la propiedad inmobiliaria.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "title": "Título de la propiedad",
     *         "description": "Descripción de la propiedad",
     *         "photo": "URL de la foto",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 404 {
     *     "error": "Propiedad no encontrada"
     * }
     */

    public function show(string $id)
    {
        $estate = $this->set_estate($id);
        return response()->json(["data" => $estate], 200);
    }

    /**
     * Actualiza una propiedad inmobiliaria existente.
     *
     * Este endpoint actualiza una propiedad inmobiliaria existente en el sistema.
     *
     * @authenticated
     * @group Propiedades Inmobiliarias
     * @param \Illuminate\Http\Request $request
     * @param string $id El ID de la propiedad inmobiliaria.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "title": "Título de la propiedad actualizada",
     *         "description": "Descripción de la propiedad actualizada",
     *         "photo": "URL de la foto",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 404 {
     *     "error": "Propiedad no encontrada"
     * }
     */

    public function update(Request $request, string $id)
    {
        $estate = $this->set_estate($id);
        $url = $estate->photo;
        $estate->update($request->all());
        $estate->photo = $url;
        $estate->save();
        $user = User::find($estate->user_id);
        #Mail::to($user->email)->send(new EstateUpdatedMailable($user, $estate));
        if ($request->file('photo')) {
            $url = $this->upload_photo($request);
            $estate->photo = $url->original["url"];
            $estate->save();
        }
        return response()->json(["data" => $estate], 200);
    }

    /**
     * Elimina una propiedad inmobiliaria.
     *
     * Este endpoint elimina o desactiva una propiedad inmobiliaria del sistema.
     *
     * @authenticated
     * @group Propiedades Inmobiliarias
     * @param string $id El ID de la propiedad inmobiliaria.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "title": "Título de la propiedad",
     *         "description": "Descripción de la propiedad",
     *         "photo": "URL de la foto",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 404 {
     *     "error": "Propiedad no encontrada"
     * }
     * @response 500 {
     *     "error": "Descripción del error"
     * }
     */

    public function destroy(string $id)
    {
        try {
            $estate = $this->set_estate($id);
            ($estate->active) ? $estate->active = false : $estate->active = true;
            if ($estate->save()) {
                return response()->json(["data" => $estate], 200);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e], 500);
        }
    }

    /**
     * Establece una propiedad inmobiliaria por su ID.
     *
     * Este método establece una propiedad inmobiliaria específica por su ID.
     *
     * @param string $id El ID de la propiedad inmobiliaria.
     * @return \App\Models\Estate
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */

    private function set_estate(string $id)
    {
        $estate = Estate::findOrFail($id);
        return $estate;
    }

    /**
     * Sube una foto de la propiedad inmobiliaria.
     *
     * Este método permite subir una foto de la propiedad inmobiliaria al sistema.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    private function upload_photo(Request $request)
    {
        $response = cloudinary()->upload($request->file('photo')->getRealPath())->getSecurePath();
        return response()->json(['url' => $response]);
    }
}
