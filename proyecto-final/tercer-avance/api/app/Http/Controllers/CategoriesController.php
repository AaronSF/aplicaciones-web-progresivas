<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    /**
     * Obtiene todas las categorías de productos.
     *
     * Este endpoint devuelve todas las categorías de productos disponibles.
     * Los administradores ven todas las categorías, mientras que los agricultores solo ven las activas.
     *
     * @authenticated
     * @group Categorías
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "name": "Nombre de la categoría",
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         },
     *         {
     *             "id": 2,
     *             "name": "Otra categoría",
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         }
     *     ]
     * }
     * @response 400 {
     *     "error": "Tu usuario no cuenta con un rol indicado"
     * }
     */

    public function index()
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('admin')) {
            $orders = Category::orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $orders]);
        } elseif ($user->hasRole('farmer')) {
            $orders = Category::where('active', true)
                ->orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $orders]);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Busca categorías de productos por nombre.
     *
     * Este endpoint permite buscar categorías de productos por su nombre.
     *
     * @authenticated
     * @group Categorías
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @queryParam name string required El nombre de la categoría a buscar.
     * @response {
     *     "data": [
     *         {
     *             "id": 1,
     *             "name": "Nombre de la categoría",
     *             "created_at": "Fecha de creación",
     *             "updated_at": "Fecha de actualización"
     *         }
     *     ]
     * }
     * @response 400 {
     *     "error": "No se encontró un nombre, apellido o e-mail"
     * }
     */

    public function find_category(Request $request)
    {
        if ($request->has('name')) {
            $name = $request->input('name');
            $categories = Category::whereRaw("LOWER(name) LIKE LOWER(?)", ["%{$name}%"])
                ->orderBy('name')
                ->get();
            return response()->json(['data' => $categories]);
        }
        return response()->json(['error' => "No se encontró un nombre, apellido o e-mail"], 400);
    }

    /**
     * Almacena una nueva categoría de producto.
     *
     * Este endpoint permite almacenar una nueva categoría de producto en el sistema.
     *
     * @authenticated
     * @group Categorías
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre de la categoría",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 400 {
     *     "error": "Ocurrió un error"
     * }
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $category = new Category($data);
        if ($category->save()) {
            return response()->json(["data" => $category], 200);
        } else {
            return response()->json(["error" => "Ocurrrio un error"], 400);
        }
    }

    /**
     * Muestra una categoría de producto específica.
     *
     * Este endpoint muestra los detalles de una categoría de producto específica.
     *
     * @authenticated
     * @group Categorías
     * @param string $id El ID de la categoría de producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre de la categoría",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 404 {
     *     "error": "Categoría no encontrada"
     * }
     */

    public function show(string $id)
    {
        $category =  $this->set_category($id);
        return response()->json(["data" => $category], 200);
    }


    /**
     * Actualiza una categoría de producto existente.
     *
     * Este endpoint actualiza una categoría de producto existente en el sistema.
     *
     * @authenticated
     * @group Categorías
     * @param \Illuminate\Http\Request $request
     * @param string $id El ID de la categoría de producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre de la categoría actualizada",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 404 {
     *     "error": "Categoría no encontrada"
     * }
     */

    public function update(Request $request, string $id)
    {
        $category =  $this->set_category($id);
        $category->update($request->all());
        return response()->json(["data" => $category], 200);
    }

    /**
     * Elimina una categoría de producto.
     *
     * Este endpoint elimina o desactiva una categoría de producto del sistema.
     *
     * @authenticated
     * @group Categorías
     * @param string $id El ID de la categoría de producto.
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": 1,
     *         "name": "Nombre de la categoría",
     *         "created_at": "Fecha de creación",
     *         "updated_at": "Fecha de actualización"
     *     }
     * }
     * @response 404 {
     *     "error": "Categoría no encontrada"
     * }
     * @response 500 {
     *     "error": "Descripción del error"
     * }
     */

    public function destroy(string $id)
    {
        try {
            $category = Category::find($id);
            ($category->active) ? $category->active = false : $category->active = true;
            if ($category->save()) {
                return response()->json(["data" => $category], 200);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e], 500);
        }
    }

    /**
     * Obtiene una categoría de producto por su ID.
     *
     * Este método obtiene una categoría de producto específica por su ID.
     *
     * @param string $id El ID de la categoría de producto.
     * @return \App\Models\Category
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */

    private function set_category(string $id)
    {
        $category = Category::findOrFail($id);
        return $category;
    }
}
