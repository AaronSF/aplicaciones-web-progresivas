<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SuggestedProduct;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class SuggestedProductsController extends Controller
{
    /**
     * Muestra una lista de todos los productos sugeridos.
     * @group Sugerencia de productos
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": [
     *         {
     *             "id": "ID del producto sugerido",
     *             "user_id": "ID del usuario que sugirió el producto",
     *             "name": "Nombre del producto sugerido",
     *             "description": "Descripción del producto sugerido",
     *             "created_at": "Fecha de creación del producto sugerido",
     *             "updated_at": "Fecha de actualización del producto sugerido"
     *         },
     *         ...
     *     ]
     * }
     */

    public function index()
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('admin')) {
            $suggested_products = SuggestedProduct::orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $suggested_products]);
        } elseif ($user->hasRole('farmer')) {
            $suggested_products = SuggestedProduct::where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->get();
            return response()->json(["data" => $suggested_products]);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }

    /**
     * Almacena un nuevo producto sugerido.
     * @group Sugerencia de productos
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID del producto sugerido",
     *         "user_id": "ID del usuario que sugirió el producto",
     *         "name": "Nombre del producto sugerido",
     *         "description": "Descripción del producto sugerido",
     *         "created_at": "Fecha de creación del producto sugerido",
     *         "updated_at": "Fecha de actualización del producto sugerido"
     *     }
     * }
     * @response 400 {"error": "Ocurrrió un error al almacenar el producto sugerido."}
     */

    public function store(Request $request)
    {
        $data = $request->all();
        $userid = Auth::guard('api')->user()->id;
        $suggested_product = new SuggestedProduct($data);
        $suggested_product->user_id = $userid;
        if ($suggested_product->save()) {
            return response()->json(["data" => $suggested_product], 200);
        } else {
            return response()->json(["error" => "Ocurrrio un error"], 400);
        }
    }

    /**
     * Muestra el producto sugerido especificado.
     * @group Sugerencia de productos
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID del producto sugerido",
     *         "user_id": "ID del usuario que sugirió el producto",
     *         "name": "Nombre del producto sugerido",
     *         "description": "Descripción del producto sugerido",
     *         "created_at": "Fecha de creación del producto sugerido",
     *         "updated_at": "Fecha de actualización del producto sugerido"
     *     }
     * }
     */

    public function show(string $id)
    {
        $user = Auth::guard('api')->user();
        if ($user->hasRole('admin') || $user->hasRole('farmer')) {
            $suggested_product = $this->set_suggested_product($id);
            return response()->json(["data" => $suggested_product], 200);
        }
        return response()->json(["error" => "Tu usuario no cuenta con un rol indicado"], 400);
    }


    /**
     * Actualiza el producto sugerido especificado en el almacenamiento.
     * @group Sugerencia de productos
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID del producto sugerido",
     *         "user_id": "ID del usuario que sugirió el producto",
     *         "name": "Nombre del producto sugerido",
     *         "description": "Descripción del producto sugerido",
     *         "created_at": "Fecha de creación del producto sugerido",
     *         "updated_at": "Fecha de actualización del producto sugerido"
     *     }
     * }
     */

    public function update(Request $request, string $id)
    {
        $suggested_product = $this->set_suggested_product($id);
        $suggested_product->update($request->all());
        return response()->json(["data" => $suggested_product], 200);
    }

    /**
     * Actualiza el estado del producto sugerido especificado.
     * @group Sugerencia de productos
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {
     *     "data": {
     *         "id": "ID del producto sugerido",
     *         "user_id": "ID del usuario que sugirió el producto",
     *         "name": "Nombre del producto sugerido",
     *         "description": "Descripción del producto sugerido",
     *         "created_at": "Fecha de creación del producto sugerido",
     *         "updated_at": "Fecha de actualización del producto sugerido"
     *     }
     * }
     */

    public function update_status(string $id)
    {
        try {
            $product = SuggestedProduct::find($id);
            (!$product->finished) ? $product->finished = true : $product->finished = false;
            if ($product->save()) {
                return response()->json(["data" => $product], 200);
            }
        } catch (QueryException $e) {
            return response()->json(["error" => $e], 500);
        }
    }

    /**
     * Elimina el producto sugerido especificado del almacenamiento.
     * @group Sugerencia de productos
     * @param  string  $id
     * @return \Illuminate\Http\JsonResponse
     * @response {"data": "Sugerencia eliminada"}
     */

    public function destroy(string $id)
    {
        $suggested_product =  $this->set_suggested_product($id);
        $suggested_product->delete();
        return response()->json(["data" => "Sugerencia eliminada"], 200);
    }

    /**
     * Establece el producto sugerido especificado.
     * @param  string  $id
     * @return \App\Models\SuggestedProduct
     */

    private function set_suggested_product(string $id)
    {
        $suggested_product = SuggestedProduct::findOrFail($id);
        return $suggested_product;
    }
}
