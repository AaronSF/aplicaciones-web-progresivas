<!DOCTYPE html>
<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
  <meta charset="utf-8">
  <meta name="x-apple-disable-message-reformatting">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no, date=no, address=no, email=no, url=no">
  <meta name="color-scheme" content="light dark">
  <meta name="supported-color-schemes" content="light dark">
  <!--[if mso]>
  <noscript>
    <xml>
      <o:OfficeDocumentSettings xmlns:o="urn:schemas-microsoft-com:office:office">
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
  </noscript>
  <style>
    td,th,div,p,a,h1,h2,h3,h4,h5,h6 {font-family: "Segoe UI", sans-serif; mso-line-height-rule: exactly;}
  </style>
  <![endif]-->
  <style>
    @media (max-width: 600px) {
      .sm-px-4 {
        padding-left: 16px !important;
        padding-right: 16px !important
      }
    }
  </style>
</head>
<body style="margin: 0; width: 100%; padding: 0; -webkit-font-smoothing: antialiased; word-break: break-word">
  <div role="article" aria-roledescription="email" aria-label lang="en">
    <div class="sm-px-4" style="background-color: #f8fafc; font-family: ui-sans-serif, system-ui, -apple-system, 'Segoe UI', sans-serif">
      <table align="center" cellpadding="0" cellspacing="0" role="none">
        <tr>
          <td style="width: 650px; max-width: 100%">
            <div>
              <table width="100%" cellspacing="0" cellpadding="0" style="margin-top: 20px; margin-bottom: 20px" role="none">
                <tr>
                  <td align="center" style="margin-top: 20px; margin-bottom: 20px;">
                    <a href="" style="display: block;">
                      <img src="https://i.ibb.co/P15rLJr/horizontal-logo.png" alt="Agrimarket" style="max-width: 100%; vertical-align: middle; line-height: 1; border: 0; width: 300px">
                    </a>
                  </td>
                </tr>
              </table>
              <table style="width: 100%;" cellpadding="0" cellspacing="0" role="none">
                <tr>
                  <td style="border-radius: 8px; background-color: #fff; padding: 40px; font-size: 16px; color: #334155; box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05)">
                    <table align="center" class="email-body_inner" cellpadding="0" cellspacing="0" role="none" style="margin-left: auto; margin-right: auto; width: 100%; background-color: #fff">
                      <tr>
                        <td>
                          <div style="font-size: 16px;">
                            <h1 style="margin-top: 0; text-align: left; font-size: 20px; font-weight: 600; color: #111827">
                              ¡{{ $user->first_name }}, alguien solicitó tu producto!
                            </h1>
                            
                            
                            <p style="margin-top: 20px; font-size: 16px; line-height: 24px">
                              Nos complace informarte que un cliente interesado ha solicitado una orden de productos a través de Agrimarket. El cliente está interesado en adquirir {{$order->quantity." ".$order->unit_of_measurement->name." de ".$order->product->product_type->name }} de tu inventario.
                              <br>
                              <br>
                            </p>
                            <p style="margin-bottom: 5px; margin-top: 12px; font-size: 16px; line-height: 24px">
                                Por favor, revisa esta solicitud y coordina los detalles de la orden directamente con el cliente lo antes posible. Esto incluye discutir el precio, la disponibilidad de los productos y el método de entrega.
                                <br>
                                <br>
                                Te recomendamos que te pongas en contacto con el cliente a la brevedad posible para garantizar una experiencia de compra fluida y satisfactoria.
                                <br>
                                <br>
                                Si necesitas algún tipo de apoyo o asistencia durante este proceso, no dudes en ponerte en contacto con nuestro equipo de soporte.
                                <br>
                                <br>
                                ¡Gracias por formar parte de Agrimarket y por contribuir a conectar a los consumidores con productos frescos y de alta calidad!
                                <br>
                            </p>
                            <p style="margin-top: 6px; font-size: 16px; line-height: 24px; margin-bottom: 0">
                              <br>
                              ¡Saludos cordiales!
                              <br>
                              El equipo de Agrimarket
                            </p>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="background-color: #fff;">
                    <img src="https://i.ibb.co/j8dFcrz/index-1.png" alt="Agrimarket" style="max-width: 100%; vertical-align: middle; line-height: 1; border: 0; width: 100%; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px">
                  </td>
                </tr>
                <tr>
                  <td style="text-align: left; font-size: 12px; font-weight: 400; color: #4B5563">
                    <p style="margin: 20px 0;">
                      Aviso de Confidencialidad: Este correo electrónico y sus archivos adjuntos pueden contener información confidencial y privilegiada. Si usted no es el destinatario previsto, por favor notifíquelo inmediatamente al remitente y elimine este correo electrónico y cualquier archivo adjunto. No revele ni distribuya su contenido a ninguna persona, y absténgase de copiarlo o utilizarlo para cualquier propósito, ya que podría estar protegido por leyes de privacidad y derechos de propiedad intelectual. El remitente no asume ninguna responsabilidad por daños o perjuicios resultantes de la recepción o uso indebido de este correo electrónico.
                    </p>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>
</html>