<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Agrimarket API</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset("/vendor/scribe/css/theme-default.style.css") }}" media="screen">
    <link rel="stylesheet" href="{{ asset("/vendor/scribe/css/theme-default.print.css") }}" media="print">

    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>

    <link rel="stylesheet"
          href="https://unpkg.com/@highlightjs/cdn-assets@11.6.0/styles/obsidian.min.css">
    <script src="https://unpkg.com/@highlightjs/cdn-assets@11.6.0/highlight.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jets/0.14.1/jets.min.js"></script>

    <style id="language-style">
        /* starts out as display none and is replaced with js later  */
                    body .content .bash-example code { display: none; }
                    body .content .javascript-example code { display: none; }
            </style>

    <script>
        var tryItOutBaseUrl = "http://agrimarket-api.test";
        var useCsrf = Boolean();
        var csrfUrl = "/sanctum/csrf-cookie";
    </script>
    <script src="{{ asset("/vendor/scribe/js/tryitout-4.31.0.js") }}"></script>

    <script src="{{ asset("/vendor/scribe/js/theme-default-4.31.0.js") }}"></script>

</head>

<body data-languages="[&quot;bash&quot;,&quot;javascript&quot;]">

<a href="#" id="nav-button">
    <span>
        MENU
        <img src="{{ asset("/vendor/scribe/images/navbar.png") }}" alt="navbar-image"/>
    </span>
</a>
<div class="tocify-wrapper">
    <div style="background-color: #ddd; display: flex; justify-content: center; align-items: center; width: 100%;">
        <img src="img/vertical_logo.png" alt="logo" class="logo" style="padding-top: 10px; padding: 20px;" width="70%"/>
    </div>
    
            <div class="lang-selector">
                                            <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                            <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                    </div>
    
    <div class="search">
        <input type="text" class="search" id="input-search" placeholder="Search">
    </div>

    <div id="toc">
                    <ul id="tocify-header-introduction" class="tocify-header">
                <li class="tocify-item level-1" data-unique="introduction">
                    <a href="#introduction">Introducción</a>
                </li>
                            </ul>
                            </ul>
                    <ul id="tocify-header-administradores" class="tocify-header">
                <li class="tocify-item level-1" data-unique="administradores">
                    <a href="#administradores">Administradores</a>
                </li>
                                    <ul id="tocify-subheader-administradores" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="administradores-GETapi-v1-admins-dashboard">
                                <a href="#administradores-GETapi-v1-admins-dashboard">Retorna el resumen del dashboard del administrador.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="administradores-POSTapi-v1-admins-register">
                                <a href="#administradores-POSTapi-v1-admins-register">Registra un nuevo usuario administrador.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-agricultores" class="tocify-header">
                <li class="tocify-item level-1" data-unique="agricultores">
                    <a href="#agricultores">Agricultores</a>
                </li>
                                    <ul id="tocify-subheader-agricultores" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="agricultores-GETapi-v1-farmers-dashboard">
                                <a href="#agricultores-GETapi-v1-farmers-dashboard">Obtiene estadísticas del panel de control del agricultor.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="agricultores-GETapi-v1-farmers-top_sales">
                                <a href="#agricultores-GETapi-v1-farmers-top_sales">Obtiene los productos más vendidos del agricultor.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="agricultores-GETapi-v1-farmers-last_orders">
                                <a href="#agricultores-GETapi-v1-farmers-last_orders">Obtiene las últimas órdenes pendientes del agricultor.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-autenticacion" class="tocify-header">
                <li class="tocify-item level-1" data-unique="autenticacion">
                    <a href="#autenticacion">Autenticación</a>
                </li>
                                    <ul id="tocify-subheader-autenticacion" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="autenticacion-GETapi-v1-user-auth">
                                <a href="#autenticacion-GETapi-v1-user-auth">Redirecciona al proveedor de autenticación social para la autorización.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="autenticacion-GETapi-v1-user-auth-callback">
                                <a href="#autenticacion-GETapi-v1-user-auth-callback">Maneja el callback de autenticación social.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="autenticacion-POSTapi-v1-user-login">
                                <a href="#autenticacion-POSTapi-v1-user-login">Inicia sesión de usuario.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="autenticacion-POSTapi-v1-user-register">
                                <a href="#autenticacion-POSTapi-v1-user-register">Registra un nuevo usuario.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-categorias" class="tocify-header">
                <li class="tocify-item level-1" data-unique="categorias">
                    <a href="#categorias">Categorías</a>
                </li>
                                    <ul id="tocify-subheader-categorias" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="categorias-POSTapi-v1-admins-find_category">
                                <a href="#categorias-POSTapi-v1-admins-find_category">Busca categorías de productos por nombre.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="categorias-POSTapi-v1-admins-categories">
                                <a href="#categorias-POSTapi-v1-admins-categories">Almacena una nueva categoría de producto.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="categorias-GETapi-v1-admins-categories--id-">
                                <a href="#categorias-GETapi-v1-admins-categories--id-">Muestra una categoría de producto específica.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="categorias-POSTapi-v1-admins-categories--id-">
                                <a href="#categorias-POSTapi-v1-admins-categories--id-">Actualiza una categoría de producto existente.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="categorias-POSTapi-v1-admins-categories--id--active">
                                <a href="#categorias-POSTapi-v1-admins-categories--id--active">Elimina una categoría de producto.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="categorias-GETapi-v1-categories">
                                <a href="#categorias-GETapi-v1-categories">Obtiene todas las categorías de productos.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-fotos" class="tocify-header">
                <li class="tocify-item level-1" data-unique="fotos">
                    <a href="#fotos">Fotos</a>
                </li>
                                    <ul id="tocify-subheader-fotos" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="fotos-POSTapi-v1-farmers-products--id--photos">
                                <a href="#fotos-POSTapi-v1-farmers-products--id--photos">Almacena una nueva foto para un producto.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="fotos-DELETEapi-v1-farmers-products--id--photos">
                                <a href="#fotos-DELETEapi-v1-farmers-products--id--photos">Elimina una foto de un producto.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-productos" class="tocify-header">
                <li class="tocify-item level-1" data-unique="productos">
                    <a href="#productos">Productos</a>
                </li>
                                    <ul id="tocify-subheader-productos" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="productos-POSTapi-v1-farmers-products">
                                <a href="#productos-POSTapi-v1-farmers-products">Almacena un nuevo producto.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="productos-POSTapi-v1-farmers-products--id-">
                                <a href="#productos-POSTapi-v1-farmers-products--id-">Actualiza un producto existente.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="productos-POSTapi-v1-farmers-products--id--active">
                                <a href="#productos-POSTapi-v1-farmers-products--id--active">Elimina un producto existente.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="productos-GETapi-v1-products">
                                <a href="#productos-GETapi-v1-products">Muestra una lista de productos.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="productos-GETapi-v1-products--id-">
                                <a href="#productos-GETapi-v1-products--id-">Muestra un producto específico.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-propiedades-inmobiliarias" class="tocify-header">
                <li class="tocify-item level-1" data-unique="propiedades-inmobiliarias">
                    <a href="#propiedades-inmobiliarias">Propiedades Inmobiliarias</a>
                </li>
                                    <ul id="tocify-subheader-propiedades-inmobiliarias" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="propiedades-inmobiliarias-GETapi-v1-farmers-estates">
                                <a href="#propiedades-inmobiliarias-GETapi-v1-farmers-estates">Obtiene todas las propiedades inmobiliarias del usuario autenticado.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="propiedades-inmobiliarias-POSTapi-v1-farmers-estates">
                                <a href="#propiedades-inmobiliarias-POSTapi-v1-farmers-estates">Almacena una nueva propiedad inmobiliaria.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="propiedades-inmobiliarias-POSTapi-v1-farmers-estates--id-">
                                <a href="#propiedades-inmobiliarias-POSTapi-v1-farmers-estates--id-">Actualiza una propiedad inmobiliaria existente.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="propiedades-inmobiliarias-POSTapi-v1-farmers-estates--id--active">
                                <a href="#propiedades-inmobiliarias-POSTapi-v1-farmers-estates--id--active">Elimina una propiedad inmobiliaria.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="propiedades-inmobiliarias-GETapi-v1-estates--id-">
                                <a href="#propiedades-inmobiliarias-GETapi-v1-estates--id-">Muestra una propiedad inmobiliaria específica.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-sugerencia-de-productos" class="tocify-header">
                <li class="tocify-item level-1" data-unique="sugerencia-de-productos">
                    <a href="#sugerencia-de-productos">Sugerencia de productos</a>
                </li>
                                    <ul id="tocify-subheader-sugerencia-de-productos" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="sugerencia-de-productos-POSTapi-v1-admins-suggestions--id--update_status">
                                <a href="#sugerencia-de-productos-POSTapi-v1-admins-suggestions--id--update_status">Actualiza el estado del producto sugerido especificado.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="sugerencia-de-productos-POSTapi-v1-admins-suggestions--id-">
                                <a href="#sugerencia-de-productos-POSTapi-v1-admins-suggestions--id-">Actualiza el estado del producto sugerido especificado.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="sugerencia-de-productos-POSTapi-v1-farmers-suggested_products">
                                <a href="#sugerencia-de-productos-POSTapi-v1-farmers-suggested_products">Almacena un nuevo producto sugerido.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="sugerencia-de-productos-POSTapi-v1-farmers-suggested_products--id-">
                                <a href="#sugerencia-de-productos-POSTapi-v1-farmers-suggested_products--id-">Actualiza el producto sugerido especificado en el almacenamiento.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="sugerencia-de-productos-DELETEapi-v1-farmers-suggested_products--id-">
                                <a href="#sugerencia-de-productos-DELETEapi-v1-farmers-suggested_products--id-">Elimina el producto sugerido especificado del almacenamiento.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="sugerencia-de-productos-GETapi-v1-suggested_products">
                                <a href="#sugerencia-de-productos-GETapi-v1-suggested_products">Muestra una lista de todos los productos sugeridos.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="sugerencia-de-productos-GETapi-v1-suggested_products--id-">
                                <a href="#sugerencia-de-productos-GETapi-v1-suggested_products--id-">Muestra el producto sugerido especificado.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-tipos-de-productos" class="tocify-header">
                <li class="tocify-item level-1" data-unique="tipos-de-productos">
                    <a href="#tipos-de-productos">Tipos de Productos</a>
                </li>
                                    <ul id="tocify-subheader-tipos-de-productos" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="tipos-de-productos-POSTapi-v1-admins-find_product_type">
                                <a href="#tipos-de-productos-POSTapi-v1-admins-find_product_type">Busca tipos de productos por nombre.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="tipos-de-productos-POSTapi-v1-admins-product_types">
                                <a href="#tipos-de-productos-POSTapi-v1-admins-product_types">Almacena un nuevo tipo de producto.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="tipos-de-productos-GETapi-v1-admins-product_types--id-">
                                <a href="#tipos-de-productos-GETapi-v1-admins-product_types--id-">Muestra un tipo de producto específico.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="tipos-de-productos-POSTapi-v1-admins-product_types--id-">
                                <a href="#tipos-de-productos-POSTapi-v1-admins-product_types--id-">Actualiza un tipo de producto existente.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="tipos-de-productos-POSTapi-v1-admins-product_types--id--active">
                                <a href="#tipos-de-productos-POSTapi-v1-admins-product_types--id--active">Elimina un tipo de producto existente.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="tipos-de-productos-GETapi-v1-product_types">
                                <a href="#tipos-de-productos-GETapi-v1-product_types">Muestra una lista de tipos de productos.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-unidades-de-medida" class="tocify-header">
                <li class="tocify-item level-1" data-unique="unidades-de-medida">
                    <a href="#unidades-de-medida">Unidades de medida</a>
                </li>
                                    <ul id="tocify-subheader-unidades-de-medida" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="unidades-de-medida-POSTapi-v1-admins-units_of_measurements">
                                <a href="#unidades-de-medida-POSTapi-v1-admins-units_of_measurements">Almacena una nueva unidad de medida.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="unidades-de-medida-GETapi-v1-admins-units_of_measurements--id-">
                                <a href="#unidades-de-medida-GETapi-v1-admins-units_of_measurements--id-">Muestra la unidad de medida especificada.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="unidades-de-medida-POSTapi-v1-admins-units_of_measurements--id-">
                                <a href="#unidades-de-medida-POSTapi-v1-admins-units_of_measurements--id-">Actualiza la unidad de medida especificada en el almacenamiento.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="unidades-de-medida-POSTapi-v1-admins-units_of_measurements--id--active">
                                <a href="#unidades-de-medida-POSTapi-v1-admins-units_of_measurements--id--active">Elimina la unidad de medida especificada del almacenamiento.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="unidades-de-medida-GETapi-v1-units_of_measurements">
                                <a href="#unidades-de-medida-GETapi-v1-units_of_measurements">Muestra una lista de todas las unidades de medida.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-usuarios" class="tocify-header">
                <li class="tocify-item level-1" data-unique="usuarios">
                    <a href="#usuarios">Usuarios</a>
                </li>
                                    <ul id="tocify-subheader-usuarios" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="usuarios-GETapi-v1-admins-users">
                                <a href="#usuarios-GETapi-v1-admins-users">Muestra una lista de todos los usuarios.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="usuarios-POSTapi-v1-admins-find_user">
                                <a href="#usuarios-POSTapi-v1-admins-find_user">Busca usuarios por nombre, apellido o correo electrónico.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="usuarios-GETapi-v1-users-me">
                                <a href="#usuarios-GETapi-v1-users-me">Muestra el usuario autenticado.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="usuarios-POSTapi-v1-users-push_id">
                                <a href="#usuarios-POSTapi-v1-users-push_id">Almacena la push ID del usuario autenticado.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="usuarios-POSTapi-v1-users-me-update">
                                <a href="#usuarios-POSTapi-v1-users-me-update">Actualiza los datos del usuario autenticado.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="usuarios-GETapi-v1-users--id-">
                                <a href="#usuarios-GETapi-v1-users--id-">Muestra el usuario especificado.</a>
                            </li>
                                                                        </ul>
                            </ul>
                    <ul id="tocify-header-ordenes" class="tocify-header">
                <li class="tocify-item level-1" data-unique="ordenes">
                    <a href="#ordenes">Órdenes</a>
                </li>
                                    <ul id="tocify-subheader-ordenes" class="tocify-subheader">
                                                    <li class="tocify-item level-2" data-unique="ordenes-POSTapi-v1-clients-orders">
                                <a href="#ordenes-POSTapi-v1-clients-orders">Almacena una nueva orden.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="ordenes-POSTapi-v1-clients-orders--id-">
                                <a href="#ordenes-POSTapi-v1-clients-orders--id-">Actualiza una orden existente.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="ordenes-POSTapi-v1-clients-orders--id--active">
                                <a href="#ordenes-POSTapi-v1-clients-orders--id--active">Elimina una orden.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="ordenes-GETapi-v1-orders">
                                <a href="#ordenes-GETapi-v1-orders">Obtiene una lista de órdenes.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="ordenes-GETapi-v1-orders--id-">
                                <a href="#ordenes-GETapi-v1-orders--id-">Muestra una orden específica.</a>
                            </li>
                                                                                <li class="tocify-item level-2" data-unique="ordenes-POSTapi-v1-orders-update_order_status--id-">
                                <a href="#ordenes-POSTapi-v1-orders-update_order_status--id-">Actualiza el estado de una orden.</a>
                            </li>
                                                                        </ul>
                            </ul>
            </div>

    <ul class="toc-footer" id="toc-footer">
                    <li style="padding-bottom: 5px;"><a href="{{ route("scribe.postman") }}">View Postman collection</a></li>
                            <li style="padding-bottom: 5px;"><a href="{{ route("scribe.openapi") }}">View OpenAPI spec</a></li>
                <li><a href="http://github.com/knuckleswtf/scribe">Documentation powered by Scribe ✍</a></li>
    </ul>

    <ul class="toc-footer" id="last-updated">
        <li>Last updated: April 12, 2024</li>
    </ul>
</div>

<div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
        <h1 id="introduction">Introducción</h1>
<aside>
    <strong>URL</strong>: <code>http://agrimarket-api.test</code>
</aside>
<p>Bienvenido a la Agrimarket API, una plataforma diseñada para ayudar a gestionar eficientemente los diferentes aspectos relacionados con la agricultura. Esta API proporciona acceso a una variedad de recursos que abarcan desde la administración de usuarios hasta la gestión de productos y propiedades inmobiliarias.</p>
<aside>
 <p style="font-size: 30px; font-weight: 700;">Recursos Disponibles</p>
    <p style="font-size: 20px; font-weight: 700;">Administradores</p>
    <p>
        El recurso de Administradores permite realizar operaciones relacionadas con la administración de usuarios con roles de administrador en la plataforma. Desde la creación hasta la actualización y eliminación de cuentas de administradores, esta API proporciona funcionalidades completas para gestionar este tipo de usuario.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Agricultores</p>
    <p>
        El recurso de Agricultores ofrece funcionalidades para gestionar usuarios con roles de agricultor. Desde la creación de nuevas cuentas de agricultores hasta la actualización de su información personal, esta API permite gestionar eficazmente los usuarios involucrados en la agricultura.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Autenticación</p>
    <p>
        El recurso de Autenticación se encarga de las funcionalidades relacionadas con la autenticación de usuarios en la plataforma. Desde el inicio de sesión hasta el manejo de tokens de acceso, esta API proporciona mecanismos seguros para gestionar la autenticación de usuarios.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Categorías</p>
    <p>
        El recurso de Categorías permite administrar las diferentes categorías de productos disponibles en la plataforma. Desde la creación de nuevas categorías hasta la eliminación de las existentes, esta API ofrece funcionalidades completas para gestionar la taxonomía de productos.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Fotos</p>
    <p>
        El recurso de Fotos ofrece funcionalidades para gestionar las imágenes asociadas a los productos en la plataforma. Desde la carga de nuevas imágenes hasta la eliminación de las existentes, esta API permite gestionar eficazmente el contenido visual relacionado con los productos.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Productos</p>
    <p>
        El recurso de Productos proporciona funcionalidades para administrar los productos disponibles en la plataforma. Desde la creación de nuevos productos hasta la actualización y eliminación de los existentes, esta API permite gestionar eficientemente el catálogo de productos.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Propiedades Inmobiliarias</p>
    <p>
        El recurso de Propiedades Inmobiliarias ofrece funcionalidades para gestionar las propiedades relacionadas con la agricultura, como terrenos y edificaciones. Desde la creación de nuevas propiedades hasta la actualización y eliminación de las existentes, esta API facilita la gestión de activos inmobiliarios.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Sugerencia de Productos</p>
    <p>
        El recurso de Sugerencia de Productos permite a los usuarios sugerir nuevos productos para su inclusión en la plataforma. Desde la creación de nuevas sugerencias hasta su revisión y aprobación por parte de los administradores, esta API proporciona un mecanismo para ampliar el catálogo de productos disponibles.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Tipos de Productos</p>
    <p>
        El recurso de Tipos de Productos ofrece funcionalidades para gestionar los diferentes tipos de productos disponibles en la plataforma. Desde la creación de nuevos tipos hasta la actualización y eliminación de los existentes, esta API permite gestionar eficientemente la clasificación de productos.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Unidades de Medida</p>
    <p>
        El recurso de Unidades de Medida proporciona operaciones para gestionar las unidades de medida utilizadas en la plataforma. Desde la creación de nuevas unidades hasta la actualización y eliminación de las existentes, esta API facilita la gestión de las unidades de medida relacionadas con los productos agrícolas.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Usuarios</p>
    <p>
        El recurso de Usuarios permite realizar operaciones CRUD sobre los usuarios registrados en la plataforma. Desde la gestión de la información personal hasta la administración de roles y permisos, esta API proporciona funcionalidades completas para gestionar los usuarios de la plataforma.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Órdenes</p>
    <p>
        El recurso de Órdenes ofrece funcionalidades para gestionar las órdenes de compra realizadas en la plataforma. Desde la creación de nuevas órdenes hasta la actualización del estado de las existentes, esta API facilita la gestión de transacciones comerciales relacionadas con la agricultura.
    </p>

</aside>

        <h1 id="authenticating-requests">Authenticating requests</h1>
<p>This API is not authenticated.</p>

        <h1 id="administradores">Administradores</h1>

    <p>Este endpoint permite obtener un resumen del dashboard del administrador, incluyendo
el total de usuarios, el total de granjeros, el total de clientes y el total general.</p>

                                <h2 id="administradores-GETapi-v1-admins-dashboard">Retorna el resumen del dashboard del administrador.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>



<span id="example-requests-GETapi-v1-admins-dashboard">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/admins/dashboard" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/dashboard"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-admins-dashboard">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;admins&quot;: 5,
        &quot;clients&quot;: 10,
        &quot;farmers&quot;: 8,
        &quot;total&quot;: 23
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-admins-dashboard" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-admins-dashboard"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-admins-dashboard"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-admins-dashboard" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-admins-dashboard">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-admins-dashboard" data-method="GET"
      data-path="api/v1/admins/dashboard"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-admins-dashboard', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-admins-dashboard"
                    onclick="tryItOut('GETapi-v1-admins-dashboard');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-admins-dashboard"
                    onclick="cancelTryOut('GETapi-v1-admins-dashboard');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-admins-dashboard"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/admins/dashboard</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-admins-dashboard"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-admins-dashboard"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="administradores-POSTapi-v1-admins-register">Registra un nuevo usuario administrador.</h2>

<p>
</p>

<p>Este endpoint permite registrar un nuevo usuario administrador en el sistema.</p>

<span id="example-requests-POSTapi-v1-admins-register">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/register" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"email\": \"gleichner.tyshawn@example.org\",
    \"password\": \"[t\\\\\\\"[Wau&lt;\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/register"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "gleichner.tyshawn@example.org",
    "password": "[t\\\"[Wau&lt;"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-register">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;user&quot;: {
        &quot;id&quot;: 1,
        &quot;first_name&quot;: &quot;John&quot;,
        &quot;last_name&quot;: &quot;Doe&quot;,
        &quot;email&quot;: &quot;john@example.com&quot;,
        &quot;role&quot;: &quot;admin&quot;,
        &quot;access_token&quot;: &quot;...&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;message&quot;: &quot;Acceso inv&aacute;lido&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-register" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-register"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-register"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-register">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-register" data-method="POST"
      data-path="api/v1/admins/register"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-register', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-register"
                    onclick="tryItOut('POSTapi-v1-admins-register');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-register"
                    onclick="cancelTryOut('POSTapi-v1-admins-register');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-register"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/register</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-register"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-register"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>email</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="email"                data-endpoint="POSTapi-v1-admins-register"
               value="gleichner.tyshawn@example.org"
               data-component="body">
    <br>
<p>El correo electrónico del usuario. Example: <code>gleichner.tyshawn@example.org</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>password</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="password"                data-endpoint="POSTapi-v1-admins-register"
               value="[t\"[Wau<"
               data-component="body">
    <br>
<p>La contraseña del usuario. Example: <code>[t\"[Wau&lt;</code></p>
        </div>
        </form>

                <h1 id="agricultores">Agricultores</h1>

    

                                <h2 id="agricultores-GETapi-v1-farmers-dashboard">Obtiene estadísticas del panel de control del agricultor.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve estadísticas del panel de control del agricultor, como ganancias, total de órdenes completadas y total de productos.</p>

<span id="example-requests-GETapi-v1-farmers-dashboard">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/farmers/dashboard" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/dashboard"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-farmers-dashboard">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;completed_orders&quot;: 1000,
        &quot;total_orders&quot;: 50,
        &quot;products&quot;: 20
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-farmers-dashboard" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-farmers-dashboard"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-farmers-dashboard"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-farmers-dashboard" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-farmers-dashboard">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-farmers-dashboard" data-method="GET"
      data-path="api/v1/farmers/dashboard"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-farmers-dashboard', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-farmers-dashboard"
                    onclick="tryItOut('GETapi-v1-farmers-dashboard');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-farmers-dashboard"
                    onclick="cancelTryOut('GETapi-v1-farmers-dashboard');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-farmers-dashboard"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/farmers/dashboard</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-farmers-dashboard"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-farmers-dashboard"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="agricultores-GETapi-v1-farmers-top_sales">Obtiene los productos más vendidos del agricultor.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve los productos más vendidos del agricultor.</p>

<span id="example-requests-GETapi-v1-farmers-top_sales">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/farmers/top_sales" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/top_sales"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-farmers-top_sales">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;product_name&quot;: &quot;Producto 1&quot;,
            &quot;quantity&quot;: 50,
            &quot;total_sales&quot;: 1000
        },
        {
            &quot;id&quot;: 2,
            &quot;product_name&quot;: &quot;Producto 2&quot;,
            &quot;quantity&quot;: 30,
            &quot;total_sales&quot;: 800
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-farmers-top_sales" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-farmers-top_sales"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-farmers-top_sales"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-farmers-top_sales" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-farmers-top_sales">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-farmers-top_sales" data-method="GET"
      data-path="api/v1/farmers/top_sales"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-farmers-top_sales', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-farmers-top_sales"
                    onclick="tryItOut('GETapi-v1-farmers-top_sales');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-farmers-top_sales"
                    onclick="cancelTryOut('GETapi-v1-farmers-top_sales');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-farmers-top_sales"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/farmers/top_sales</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-farmers-top_sales"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-farmers-top_sales"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="agricultores-GETapi-v1-farmers-last_orders">Obtiene las últimas órdenes pendientes del agricultor.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve las últimas órdenes pendientes del agricultor.</p>

<span id="example-requests-GETapi-v1-farmers-last_orders">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/farmers/last_orders" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/last_orders"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-farmers-last_orders">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;order_number&quot;: &quot;ORD-001&quot;,
            &quot;total&quot;: 100,
            &quot;status&quot;: &quot;Pendiente&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;
        },
        {
            &quot;id&quot;: 2,
            &quot;order_number&quot;: &quot;ORD-002&quot;,
            &quot;total&quot;: 150,
            &quot;status&quot;: &quot;Pendiente&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-farmers-last_orders" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-farmers-last_orders"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-farmers-last_orders"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-farmers-last_orders" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-farmers-last_orders">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-farmers-last_orders" data-method="GET"
      data-path="api/v1/farmers/last_orders"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-farmers-last_orders', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-farmers-last_orders"
                    onclick="tryItOut('GETapi-v1-farmers-last_orders');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-farmers-last_orders"
                    onclick="cancelTryOut('GETapi-v1-farmers-last_orders');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-farmers-last_orders"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/farmers/last_orders</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-farmers-last_orders"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-farmers-last_orders"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                <h1 id="autenticacion">Autenticación</h1>

    

                                <h2 id="autenticacion-GETapi-v1-user-auth">Redirecciona al proveedor de autenticación social para la autorización.</h2>

<p>
</p>

<p>Este endpoint redirecciona al usuario al proveedor de autenticación social (Google)
para que pueda autorizar la aplicación.</p>

<span id="example-requests-GETapi-v1-user-auth">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/user/auth" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/user/auth"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-user-auth">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;url&quot;: &quot;URL de redirecci&oacute;n para la autorizaci&oacute;n&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-user-auth" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-user-auth"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-user-auth"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-user-auth" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-user-auth">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-user-auth" data-method="GET"
      data-path="api/v1/user/auth"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-user-auth', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-user-auth"
                    onclick="tryItOut('GETapi-v1-user-auth');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-user-auth"
                    onclick="cancelTryOut('GETapi-v1-user-auth');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-user-auth"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/user/auth</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-user-auth"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-user-auth"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="autenticacion-GETapi-v1-user-auth-callback">Maneja el callback de autenticación social.</h2>

<p>
</p>

<p>Este endpoint maneja la respuesta del proveedor de autenticación social después de la autorización.</p>

<span id="example-requests-GETapi-v1-user-auth-callback">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/user/auth/callback" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/user/auth/callback"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-user-auth-callback">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;user&quot;: {
        &quot;id&quot;: &quot;ID del usuario&quot;,
        &quot;email&quot;: &quot;Correo electr&oacute;nico del usuario&quot;,
        &quot;email_verified_at&quot;: &quot;Fecha de verificaci&oacute;n del correo electr&oacute;nico&quot;,
        &quot;name&quot;: &quot;Nombre del usuario&quot;,
        &quot;password&quot;: &quot;Contrase&ntilde;a del usuario&quot;,
        &quot;google_id&quot;: &quot;ID de Google del usuario&quot;,
        &quot;avatar&quot;: &quot;URL del avatar del usuario&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del usuario&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del usuario&quot;
    },
    &quot;access_token&quot;: &quot;Token de acceso del usuario&quot;,
    &quot;token_type&quot;: &quot;Bearer&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (422):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Invalid credentials provided.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-user-auth-callback" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-user-auth-callback"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-user-auth-callback"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-user-auth-callback" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-user-auth-callback">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-user-auth-callback" data-method="GET"
      data-path="api/v1/user/auth/callback"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-user-auth-callback', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-user-auth-callback"
                    onclick="tryItOut('GETapi-v1-user-auth-callback');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-user-auth-callback"
                    onclick="cancelTryOut('GETapi-v1-user-auth-callback');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-user-auth-callback"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/user/auth/callback</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-user-auth-callback"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-user-auth-callback"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="autenticacion-POSTapi-v1-user-login">Inicia sesión de usuario.</h2>

<p>
</p>

<p>Este endpoint permite a un usuario iniciar sesión en el sistema.</p>

<span id="example-requests-POSTapi-v1-user-login">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/user/login" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"email\": \"schmidt.luis@example.com\",
    \"password\": \"uIaASJW\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/user/login"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "email": "schmidt.luis@example.com",
    "password": "uIaASJW"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-user-login">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;user&quot;: {
        &quot;id&quot;: 1,
        &quot;first_name&quot;: &quot;John&quot;,
        &quot;last_name&quot;: &quot;Doe&quot;,
        &quot;email&quot;: &quot;john@example.com&quot;,
        &quot;role&quot;: &quot;client&quot;,
        &quot;access_token&quot;: &quot;...&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;message&quot;: &quot;Usuario y/o contrase&ntilde;a inv&aacute;lidos.&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (401):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;message&quot;: &quot;Error de autenticaci&oacute;n.&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;...&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-user-login" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-user-login"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-user-login"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-user-login" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-user-login">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-user-login" data-method="POST"
      data-path="api/v1/user/login"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-user-login', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-user-login"
                    onclick="tryItOut('POSTapi-v1-user-login');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-user-login"
                    onclick="cancelTryOut('POSTapi-v1-user-login');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-user-login"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/user/login</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-user-login"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-user-login"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>email</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="email"                data-endpoint="POSTapi-v1-user-login"
               value="schmidt.luis@example.com"
               data-component="body">
    <br>
<p>El correo electrónico del usuario. Example: <code>schmidt.luis@example.com</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>password</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="password"                data-endpoint="POSTapi-v1-user-login"
               value="uIaASJW"
               data-component="body">
    <br>
<p>La contraseña del usuario. Example: <code>uIaASJW</code></p>
        </div>
        </form>

                    <h2 id="autenticacion-POSTapi-v1-user-register">Registra un nuevo usuario.</h2>

<p>
</p>

<p>Este endpoint permite registrar un nuevo usuario en el sistema.</p>

<span id="example-requests-POSTapi-v1-user-register">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/user/register" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"first_name\": \"aperiam\",
    \"last_name\": \"animi\",
    \"email\": \"vicky35@example.org\",
    \"password\": \"EC=lTwqZ\",
    \"google_id\": \"magni\",
    \"type\": \"quam\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/user/register"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "first_name": "aperiam",
    "last_name": "animi",
    "email": "vicky35@example.org",
    "password": "EC=lTwqZ",
    "google_id": "magni",
    "type": "quam"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-user-register">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;user&quot;: {
        &quot;id&quot;: 1,
        &quot;first_name&quot;: &quot;John&quot;,
        &quot;last_name&quot;: &quot;Doe&quot;,
        &quot;email&quot;: &quot;john@example.com&quot;,
        &quot;role&quot;: &quot;client&quot;,
        &quot;access_token&quot;: &quot;...&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;...&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-user-register" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-user-register"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-user-register"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-user-register" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-user-register">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-user-register" data-method="POST"
      data-path="api/v1/user/register"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-user-register', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-user-register"
                    onclick="tryItOut('POSTapi-v1-user-register');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-user-register"
                    onclick="cancelTryOut('POSTapi-v1-user-register');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-user-register"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/user/register</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-user-register"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-user-register"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>first_name</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="first_name"                data-endpoint="POSTapi-v1-user-register"
               value="aperiam"
               data-component="body">
    <br>
<p>El nombre del usuario. Example: <code>aperiam</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>last_name</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="last_name"                data-endpoint="POSTapi-v1-user-register"
               value="animi"
               data-component="body">
    <br>
<p>El apellido del usuario. Example: <code>animi</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>email</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="email"                data-endpoint="POSTapi-v1-user-register"
               value="vicky35@example.org"
               data-component="body">
    <br>
<p>El correo electrónico del usuario. Example: <code>vicky35@example.org</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>password</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="password"                data-endpoint="POSTapi-v1-user-register"
               value="EC=lTwqZ"
               data-component="body">
    <br>
<p>La contraseña del usuario. Example: <code>EC=lTwqZ</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>google_id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
<i>optional</i> &nbsp;
                <input type="text" style="display: none"
                              name="google_id"                data-endpoint="POSTapi-v1-user-register"
               value="magni"
               data-component="body">
    <br>
<p>El ID de Google del usuario (opcional). Example: <code>magni</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>type</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="type"                data-endpoint="POSTapi-v1-user-register"
               value="quam"
               data-component="body">
    <br>
<p>El tipo de usuario (2 para agricultor, cualquier otro valor para cliente). Example: <code>quam</code></p>
        </div>
        </form>

                <h1 id="categorias">Categorías</h1>

    

                                <h2 id="categorias-POSTapi-v1-admins-find_category">Busca categorías de productos por nombre.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite buscar categorías de productos por su nombre.</p>

<span id="example-requests-POSTapi-v1-admins-find_category">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/find_category?name=sint" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/find_category"
);

const params = {
    "name": "sint",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-find_category">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;name&quot;: &quot;Nombre de la categor&iacute;a&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        }
    ]
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;No se encontr&oacute; un nombre, apellido o e-mail&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-find_category" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-find_category"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-find_category"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-find_category" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-find_category">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-find_category" data-method="POST"
      data-path="api/v1/admins/find_category"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-find_category', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-find_category"
                    onclick="tryItOut('POSTapi-v1-admins-find_category');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-find_category"
                    onclick="cancelTryOut('POSTapi-v1-admins-find_category');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-find_category"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/find_category</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-find_category"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-find_category"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                            <h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
                                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>name</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="name"                data-endpoint="POSTapi-v1-admins-find_category"
               value="sint"
               data-component="query">
    <br>
<p>El nombre de la categoría a buscar. Example: <code>sint</code></p>
            </div>
                </form>

                    <h2 id="categorias-POSTapi-v1-admins-categories">Almacena una nueva categoría de producto.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite almacenar una nueva categoría de producto en el sistema.</p>

<span id="example-requests-POSTapi-v1-admins-categories">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/categories" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/categories"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-categories">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre de la categor&iacute;a&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurri&oacute; un error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-categories" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-categories"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-categories"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-categories" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-categories">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-categories" data-method="POST"
      data-path="api/v1/admins/categories"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-categories', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-categories"
                    onclick="tryItOut('POSTapi-v1-admins-categories');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-categories"
                    onclick="cancelTryOut('POSTapi-v1-admins-categories');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-categories"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/categories</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-categories"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-categories"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="categorias-GETapi-v1-admins-categories--id-">Muestra una categoría de producto específica.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint muestra los detalles de una categoría de producto específica.</p>

<span id="example-requests-GETapi-v1-admins-categories--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/admins/categories/harum" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/categories/harum"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-admins-categories--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre de la categor&iacute;a&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Categor&iacute;a no encontrada&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-admins-categories--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-admins-categories--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-admins-categories--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-admins-categories--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-admins-categories--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-admins-categories--id-" data-method="GET"
      data-path="api/v1/admins/categories/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-admins-categories--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-admins-categories--id-"
                    onclick="tryItOut('GETapi-v1-admins-categories--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-admins-categories--id-"
                    onclick="cancelTryOut('GETapi-v1-admins-categories--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-admins-categories--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/admins/categories/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-admins-categories--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-admins-categories--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-admins-categories--id-"
               value="harum"
               data-component="url">
    <br>
<p>The ID of the category. Example: <code>harum</code></p>
            </div>
                    </form>

                    <h2 id="categorias-POSTapi-v1-admins-categories--id-">Actualiza una categoría de producto existente.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint actualiza una categoría de producto existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-admins-categories--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/categories/animi" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/categories/animi"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-categories--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre de la categor&iacute;a actualizada&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Categor&iacute;a no encontrada&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-categories--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-categories--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-categories--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-categories--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-categories--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-categories--id-" data-method="POST"
      data-path="api/v1/admins/categories/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-categories--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-categories--id-"
                    onclick="tryItOut('POSTapi-v1-admins-categories--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-categories--id-"
                    onclick="cancelTryOut('POSTapi-v1-admins-categories--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-categories--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/categories/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-categories--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-categories--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-categories--id-"
               value="animi"
               data-component="url">
    <br>
<p>The ID of the category. Example: <code>animi</code></p>
            </div>
                    </form>

                    <h2 id="categorias-POSTapi-v1-admins-categories--id--active">Elimina una categoría de producto.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint elimina o desactiva una categoría de producto del sistema.</p>

<span id="example-requests-POSTapi-v1-admins-categories--id--active">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/categories/omnis/active" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/categories/omnis/active"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-categories--id--active">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre de la categor&iacute;a&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Categor&iacute;a no encontrada&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-categories--id--active" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-categories--id--active"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-categories--id--active"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-categories--id--active" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-categories--id--active">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-categories--id--active" data-method="POST"
      data-path="api/v1/admins/categories/{id}/active"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-categories--id--active', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-categories--id--active"
                    onclick="tryItOut('POSTapi-v1-admins-categories--id--active');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-categories--id--active"
                    onclick="cancelTryOut('POSTapi-v1-admins-categories--id--active');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-categories--id--active"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/categories/{id}/active</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-categories--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-categories--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-categories--id--active"
               value="omnis"
               data-component="url">
    <br>
<p>The ID of the category. Example: <code>omnis</code></p>
            </div>
                    </form>

                    <h2 id="categorias-GETapi-v1-categories">Obtiene todas las categorías de productos.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve todas las categorías de productos disponibles.
Los administradores ven todas las categorías, mientras que los agricultores solo ven las activas.</p>

<span id="example-requests-GETapi-v1-categories">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/categories" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/categories"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-categories">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;name&quot;: &quot;Nombre de la categor&iacute;a&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        },
        {
            &quot;id&quot;: 2,
            &quot;name&quot;: &quot;Otra categor&iacute;a&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        }
    ]
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Tu usuario no cuenta con un rol indicado&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-categories" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-categories"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-categories"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-categories" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-categories">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-categories" data-method="GET"
      data-path="api/v1/categories"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-categories', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-categories"
                    onclick="tryItOut('GETapi-v1-categories');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-categories"
                    onclick="cancelTryOut('GETapi-v1-categories');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-categories"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/categories</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-categories"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-categories"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                <h1 id="fotos">Fotos</h1>

    

                                <h2 id="fotos-POSTapi-v1-farmers-products--id--photos">Almacena una nueva foto para un producto.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite almacenar una nueva foto para un producto específico.</p>

<span id="example-requests-POSTapi-v1-farmers-products--id--photos">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/products/doloremque/photos" \
    --header "Content-Type: multipart/form-data" \
    --header "Accept: application/json" \
    --form "photo=@/tmp/php5TO1Mn" </code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/products/doloremque/photos"
);

const headers = {
    "Content-Type": "multipart/form-data",
    "Accept": "application/json",
};

const body = new FormData();
body.append('photo', document.querySelector('input[name="photo"]').files[0]);

fetch(url, {
    method: "POST",
    headers,
    body,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-products--id--photos">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;product_id&quot;: 1,
        &quot;photo&quot;: &quot;URL de la foto&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurrrio un error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-products--id--photos" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-products--id--photos"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-products--id--photos"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-products--id--photos" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-products--id--photos">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-products--id--photos" data-method="POST"
      data-path="api/v1/farmers/products/{id}/photos"
      data-authed="1"
      data-hasfiles="1"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-products--id--photos', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-products--id--photos"
                    onclick="tryItOut('POSTapi-v1-farmers-products--id--photos');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-products--id--photos"
                    onclick="cancelTryOut('POSTapi-v1-farmers-products--id--photos');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-products--id--photos"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/products/{id}/photos</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-products--id--photos"
               value="multipart/form-data"
               data-component="header">
    <br>
<p>Example: <code>multipart/form-data</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-products--id--photos"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-farmers-products--id--photos"
               value="doloremque"
               data-component="url">
    <br>
<p>El ID del producto. Example: <code>doloremque</code></p>
            </div>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>photo</code></b>&nbsp;&nbsp;
<small>file</small>&nbsp;
 &nbsp;
                <input type="file" style="display: none"
                              name="photo"                data-endpoint="POSTapi-v1-farmers-products--id--photos"
               value=""
               data-component="body">
    <br>
<p>La foto del producto a subir. Example: <code>/tmp/php5TO1Mn</code></p>
        </div>
        </form>

                    <h2 id="fotos-DELETEapi-v1-farmers-products--id--photos">Elimina una foto de un producto.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite eliminar una foto de un producto existente en el sistema.</p>

<span id="example-requests-DELETEapi-v1-farmers-products--id--photos">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request DELETE \
    "http://agrimarket-api.test/api/v1/farmers/products/rerum/photos" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/products/rerum/photos"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-DELETEapi-v1-farmers-products--id--photos">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: &quot;Foto eliminada&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-DELETEapi-v1-farmers-products--id--photos" hidden>
    <blockquote>Received response<span
                id="execution-response-status-DELETEapi-v1-farmers-products--id--photos"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-v1-farmers-products--id--photos"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-DELETEapi-v1-farmers-products--id--photos" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-v1-farmers-products--id--photos">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-DELETEapi-v1-farmers-products--id--photos" data-method="DELETE"
      data-path="api/v1/farmers/products/{id}/photos"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('DELETEapi-v1-farmers-products--id--photos', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-DELETEapi-v1-farmers-products--id--photos"
                    onclick="tryItOut('DELETEapi-v1-farmers-products--id--photos');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-DELETEapi-v1-farmers-products--id--photos"
                    onclick="cancelTryOut('DELETEapi-v1-farmers-products--id--photos');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-DELETEapi-v1-farmers-products--id--photos"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-red">DELETE</small>
            <b><code>api/v1/farmers/products/{id}/photos</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="DELETEapi-v1-farmers-products--id--photos"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="DELETEapi-v1-farmers-products--id--photos"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="DELETEapi-v1-farmers-products--id--photos"
               value="rerum"
               data-component="url">
    <br>
<p>El ID de la foto. Example: <code>rerum</code></p>
            </div>
                    </form>

                <h1 id="productos">Productos</h1>

    

                                <h2 id="productos-POSTapi-v1-farmers-products">Almacena un nuevo producto.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite a un usuario almacenar un nuevo producto en el sistema.</p>

<span id="example-requests-POSTapi-v1-farmers-products">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/products" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"product_type_id\": 4,
    \"description\": \"Qui omnis labore exercitationem est inventore omnis consectetur.\",
    \"price_per_measure\": 192.8675,
    \"unit_of_measurement_id\": 4,
    \"minimum_sale\": 1,
    \"cutoff_date\": \"quis\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/products"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "product_type_id": 4,
    "description": "Qui omnis labore exercitationem est inventore omnis consectetur.",
    "price_per_measure": 192.8675,
    "unit_of_measurement_id": 4,
    "minimum_sale": 1,
    "cutoff_date": "quis"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-products">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;user_id&quot;: &quot;Nombre del usuario&quot;,
        &quot;product&quot;: &quot;Nombre del producto&quot;,
        &quot;price&quot;: 10.99,
        &quot;measure&quot;: &quot;Unidad de medida&quot;,
        &quot;minimum_sale&quot;: 5,
        &quot;cutoff_date&quot;: &quot;Fecha de corte&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurrrio un error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-products" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-products"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-products"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-products" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-products">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-products" data-method="POST"
      data-path="api/v1/farmers/products"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-products', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-products"
                    onclick="tryItOut('POSTapi-v1-farmers-products');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-products"
                    onclick="cancelTryOut('POSTapi-v1-farmers-products');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-products"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/products</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>product_type_id</code></b>&nbsp;&nbsp;
<small>integer</small>&nbsp;
 &nbsp;
                <input type="number" style="display: none"
               step="any"               name="product_type_id"                data-endpoint="POSTapi-v1-farmers-products"
               value="4"
               data-component="body">
    <br>
<p>El ID del tipo de producto. Example: <code>4</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>description</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
<i>optional</i> &nbsp;
                <input type="text" style="display: none"
                              name="description"                data-endpoint="POSTapi-v1-farmers-products"
               value="Qui omnis labore exercitationem est inventore omnis consectetur."
               data-component="body">
    <br>
<p>Descripción del producto. Example: <code>Qui omnis labore exercitationem est inventore omnis consectetur.</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>price_per_measure</code></b>&nbsp;&nbsp;
<small>number</small>&nbsp;
 &nbsp;
                <input type="number" style="display: none"
               step="any"               name="price_per_measure"                data-endpoint="POSTapi-v1-farmers-products"
               value="192.8675"
               data-component="body">
    <br>
<p>Precio por unidad de medida. Example: <code>192.8675</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>unit_of_measurement_id</code></b>&nbsp;&nbsp;
<small>integer</small>&nbsp;
 &nbsp;
                <input type="number" style="display: none"
               step="any"               name="unit_of_measurement_id"                data-endpoint="POSTapi-v1-farmers-products"
               value="4"
               data-component="body">
    <br>
<p>El ID de la unidad de medida. Example: <code>4</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>minimum_sale</code></b>&nbsp;&nbsp;
<small>integer</small>&nbsp;
<i>optional</i> &nbsp;
                <input type="number" style="display: none"
               step="any"               name="minimum_sale"                data-endpoint="POSTapi-v1-farmers-products"
               value="1"
               data-component="body">
    <br>
<p>Cantidad mínima de venta. Example: <code>1</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>cutoff_date</code></b>&nbsp;&nbsp;
<small>date</small>&nbsp;
<i>optional</i> &nbsp;
                <input type="text" style="display: none"
                              name="cutoff_date"                data-endpoint="POSTapi-v1-farmers-products"
               value="quis"
               data-component="body">
    <br>
<p>Fecha de corte. Example: <code>quis</code></p>
        </div>
        </form>

                    <h2 id="productos-POSTapi-v1-farmers-products--id-">Actualiza un producto existente.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite actualizar la información de un producto existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-farmers-products--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/products/voluptas" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/products/voluptas"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-products--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;user_id&quot;: &quot;Nombre del usuario&quot;,
        &quot;product&quot;: &quot;Nombre del producto&quot;,
        &quot;price&quot;: 10.99,
        &quot;measure&quot;: &quot;Unidad de medida&quot;,
        &quot;minimum_sale&quot;: 5,
        &quot;cutoff_date&quot;: &quot;Fecha de corte&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Tu usuario no cuenta con un rol indicado&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-products--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-products--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-products--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-products--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-products--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-products--id-" data-method="POST"
      data-path="api/v1/farmers/products/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-products--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-products--id-"
                    onclick="tryItOut('POSTapi-v1-farmers-products--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-products--id-"
                    onclick="cancelTryOut('POSTapi-v1-farmers-products--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-products--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/products/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-farmers-products--id-"
               value="voluptas"
               data-component="url">
    <br>
<p>El ID del producto. Example: <code>voluptas</code></p>
            </div>
                    </form>

                    <h2 id="productos-POSTapi-v1-farmers-products--id--active">Elimina un producto existente.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite eliminar un producto existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-farmers-products--id--active">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/products/in/active" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/products/in/active"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-products--id--active">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;user_id&quot;: &quot;Nombre del usuario&quot;,
        &quot;product&quot;: &quot;Nombre del producto&quot;,
        &quot;price&quot;: 10.99,
        &quot;measure&quot;: &quot;Unidad de medida&quot;,
        &quot;minimum_sale&quot;: 5,
        &quot;cutoff_date&quot;: &quot;Fecha de corte&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-products--id--active" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-products--id--active"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-products--id--active"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-products--id--active" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-products--id--active">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-products--id--active" data-method="POST"
      data-path="api/v1/farmers/products/{id}/active"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-products--id--active', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-products--id--active"
                    onclick="tryItOut('POSTapi-v1-farmers-products--id--active');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-products--id--active"
                    onclick="cancelTryOut('POSTapi-v1-farmers-products--id--active');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-products--id--active"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/products/{id}/active</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-products--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-products--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-farmers-products--id--active"
               value="in"
               data-component="url">
    <br>
<p>El ID del producto. Example: <code>in</code></p>
            </div>
                    </form>

                    <h2 id="productos-GETapi-v1-products">Muestra una lista de productos.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve una lista de productos disponibles en el sistema.</p>

<span id="example-requests-GETapi-v1-products">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/products" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/products"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-products">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;user_id&quot;: &quot;Nombre del usuario&quot;,
            &quot;product&quot;: &quot;Nombre del producto&quot;,
            &quot;price&quot;: 10.99,
            &quot;measure&quot;: &quot;Unidad de medida&quot;,
            &quot;minimum_sale&quot;: 5,
            &quot;cutoff_date&quot;: &quot;Fecha de corte&quot;,
            &quot;photos&quot;: [
                {&quot;photo&quot;: &quot;URL de la foto 1&quot;},
                {&quot;photo&quot;: &quot;URL de la foto 2&quot;}
            ],
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        },
        {
            ...
        }
    ]
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Tu usuario no cuenta con un rol indicado&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-products" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-products"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-products"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-products" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-products">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-products" data-method="GET"
      data-path="api/v1/products"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-products', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-products"
                    onclick="tryItOut('GETapi-v1-products');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-products"
                    onclick="cancelTryOut('GETapi-v1-products');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-products"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/products</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="productos-GETapi-v1-products--id-">Muestra un producto específico.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve información detallada sobre un producto específico.</p>

<span id="example-requests-GETapi-v1-products--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/products/quae" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/products/quae"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-products--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;user_id&quot;: &quot;Nombre del usuario&quot;,
        &quot;product&quot;: &quot;Nombre del producto&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n del producto&quot;,
        &quot;price&quot;: 10.99,
        &quot;measure&quot;: &quot;Unidad de medida&quot;,
        &quot;stock&quot;: 100,
        &quot;minimum_sale&quot;: 5,
        &quot;cutoff_date&quot;: &quot;Fecha de corte&quot;,
        &quot;photos&quot;: [
            {
                &quot;photo&quot;: &quot;URL de la foto 1&quot;
            },
            {
                &quot;photo&quot;: &quot;URL de la foto 2&quot;
            }
        ],
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Tu usuario no cuenta con un rol indicado&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-products--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-products--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-products--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-products--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-products--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-products--id-" data-method="GET"
      data-path="api/v1/products/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-products--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-products--id-"
                    onclick="tryItOut('GETapi-v1-products--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-products--id-"
                    onclick="cancelTryOut('GETapi-v1-products--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-products--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/products/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-products--id-"
               value="quae"
               data-component="url">
    <br>
<p>El ID del producto. Example: <code>quae</code></p>
            </div>
                    </form>

                <h1 id="propiedades-inmobiliarias">Propiedades Inmobiliarias</h1>

    

                                <h2 id="propiedades-inmobiliarias-GETapi-v1-farmers-estates">Obtiene todas las propiedades inmobiliarias del usuario autenticado.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve todas las propiedades inmobiliarias activas del usuario autenticado.</p>

<span id="example-requests-GETapi-v1-farmers-estates">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/farmers/estates" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/estates"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-farmers-estates">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;title&quot;: &quot;T&iacute;tulo de la propiedad&quot;,
            &quot;description&quot;: &quot;Descripci&oacute;n de la propiedad&quot;,
            &quot;photo&quot;: &quot;URL de la foto&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        },
        {
            &quot;id&quot;: 2,
            &quot;title&quot;: &quot;Otra propiedad&quot;,
            &quot;description&quot;: &quot;Otra descripci&oacute;n&quot;,
            &quot;photo&quot;: &quot;URL de la foto&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-farmers-estates" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-farmers-estates"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-farmers-estates"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-farmers-estates" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-farmers-estates">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-farmers-estates" data-method="GET"
      data-path="api/v1/farmers/estates"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-farmers-estates', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-farmers-estates"
                    onclick="tryItOut('GETapi-v1-farmers-estates');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-farmers-estates"
                    onclick="cancelTryOut('GETapi-v1-farmers-estates');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-farmers-estates"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/farmers/estates</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-farmers-estates"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-farmers-estates"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="propiedades-inmobiliarias-POSTapi-v1-farmers-estates">Almacena una nueva propiedad inmobiliaria.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite almacenar una nueva propiedad inmobiliaria en el sistema.</p>

<span id="example-requests-POSTapi-v1-farmers-estates">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/estates" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/estates"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-estates">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;title&quot;: &quot;T&iacute;tulo de la propiedad&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n de la propiedad&quot;,
        &quot;photo&quot;: &quot;URL de la foto&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurri&oacute; un error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-estates" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-estates"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-estates"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-estates" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-estates">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-estates" data-method="POST"
      data-path="api/v1/farmers/estates"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-estates', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-estates"
                    onclick="tryItOut('POSTapi-v1-farmers-estates');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-estates"
                    onclick="cancelTryOut('POSTapi-v1-farmers-estates');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-estates"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/estates</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-estates"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-estates"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="propiedades-inmobiliarias-POSTapi-v1-farmers-estates--id-">Actualiza una propiedad inmobiliaria existente.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint actualiza una propiedad inmobiliaria existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-farmers-estates--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/estates/rerum" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/estates/rerum"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-estates--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;title&quot;: &quot;T&iacute;tulo de la propiedad actualizada&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n de la propiedad actualizada&quot;,
        &quot;photo&quot;: &quot;URL de la foto&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Propiedad no encontrada&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-estates--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-estates--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-estates--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-estates--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-estates--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-estates--id-" data-method="POST"
      data-path="api/v1/farmers/estates/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-estates--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-estates--id-"
                    onclick="tryItOut('POSTapi-v1-farmers-estates--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-estates--id-"
                    onclick="cancelTryOut('POSTapi-v1-farmers-estates--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-estates--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/estates/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-estates--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-estates--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-farmers-estates--id-"
               value="rerum"
               data-component="url">
    <br>
<p>The ID of the estate. Example: <code>rerum</code></p>
            </div>
                    </form>

                    <h2 id="propiedades-inmobiliarias-POSTapi-v1-farmers-estates--id--active">Elimina una propiedad inmobiliaria.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint elimina o desactiva una propiedad inmobiliaria del sistema.</p>

<span id="example-requests-POSTapi-v1-farmers-estates--id--active">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/estates/sit/active" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/estates/sit/active"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-estates--id--active">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;title&quot;: &quot;T&iacute;tulo de la propiedad&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n de la propiedad&quot;,
        &quot;photo&quot;: &quot;URL de la foto&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Propiedad no encontrada&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-estates--id--active" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-estates--id--active"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-estates--id--active"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-estates--id--active" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-estates--id--active">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-estates--id--active" data-method="POST"
      data-path="api/v1/farmers/estates/{id}/active"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-estates--id--active', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-estates--id--active"
                    onclick="tryItOut('POSTapi-v1-farmers-estates--id--active');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-estates--id--active"
                    onclick="cancelTryOut('POSTapi-v1-farmers-estates--id--active');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-estates--id--active"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/estates/{id}/active</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-estates--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-estates--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-farmers-estates--id--active"
               value="sit"
               data-component="url">
    <br>
<p>The ID of the estate. Example: <code>sit</code></p>
            </div>
                    </form>

                    <h2 id="propiedades-inmobiliarias-GETapi-v1-estates--id-">Muestra una propiedad inmobiliaria específica.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint muestra los detalles de una propiedad inmobiliaria específica.</p>

<span id="example-requests-GETapi-v1-estates--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/estates/magni" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/estates/magni"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-estates--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;title&quot;: &quot;T&iacute;tulo de la propiedad&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n de la propiedad&quot;,
        &quot;photo&quot;: &quot;URL de la foto&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (404):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Propiedad no encontrada&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-estates--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-estates--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-estates--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-estates--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-estates--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-estates--id-" data-method="GET"
      data-path="api/v1/estates/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-estates--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-estates--id-"
                    onclick="tryItOut('GETapi-v1-estates--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-estates--id-"
                    onclick="cancelTryOut('GETapi-v1-estates--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-estates--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/estates/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-estates--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-estates--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-estates--id-"
               value="magni"
               data-component="url">
    <br>
<p>The ID of the estate. Example: <code>magni</code></p>
            </div>
                    </form>

                <h1 id="sugerencia-de-productos">Sugerencia de productos</h1>

    

                                <h2 id="sugerencia-de-productos-POSTapi-v1-admins-suggestions--id--update_status">Actualiza el estado del producto sugerido especificado.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-admins-suggestions--id--update_status">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/suggestions/non/update_status" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/suggestions/non/update_status"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-suggestions--id--update_status">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del producto sugerido&quot;,
        &quot;user_id&quot;: &quot;ID del usuario que sugiri&oacute; el producto&quot;,
        &quot;name&quot;: &quot;Nombre del producto sugerido&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n del producto sugerido&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del producto sugerido&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del producto sugerido&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-suggestions--id--update_status" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-suggestions--id--update_status"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-suggestions--id--update_status"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-suggestions--id--update_status" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-suggestions--id--update_status">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-suggestions--id--update_status" data-method="POST"
      data-path="api/v1/admins/suggestions/{id}/update_status"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-suggestions--id--update_status', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-suggestions--id--update_status"
                    onclick="tryItOut('POSTapi-v1-admins-suggestions--id--update_status');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-suggestions--id--update_status"
                    onclick="cancelTryOut('POSTapi-v1-admins-suggestions--id--update_status');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-suggestions--id--update_status"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/suggestions/{id}/update_status</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-suggestions--id--update_status"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-suggestions--id--update_status"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-suggestions--id--update_status"
               value="non"
               data-component="url">
    <br>
<p>The ID of the suggestion. Example: <code>non</code></p>
            </div>
                    </form>

                    <h2 id="sugerencia-de-productos-POSTapi-v1-admins-suggestions--id-">Actualiza el estado del producto sugerido especificado.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-admins-suggestions--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/suggestions/deleniti" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/suggestions/deleniti"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-suggestions--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del producto sugerido&quot;,
        &quot;user_id&quot;: &quot;ID del usuario que sugiri&oacute; el producto&quot;,
        &quot;name&quot;: &quot;Nombre del producto sugerido&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n del producto sugerido&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del producto sugerido&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del producto sugerido&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-suggestions--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-suggestions--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-suggestions--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-suggestions--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-suggestions--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-suggestions--id-" data-method="POST"
      data-path="api/v1/admins/suggestions/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-suggestions--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-suggestions--id-"
                    onclick="tryItOut('POSTapi-v1-admins-suggestions--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-suggestions--id-"
                    onclick="cancelTryOut('POSTapi-v1-admins-suggestions--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-suggestions--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/suggestions/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-suggestions--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-suggestions--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-suggestions--id-"
               value="deleniti"
               data-component="url">
    <br>
<p>The ID of the suggestion. Example: <code>deleniti</code></p>
            </div>
                    </form>

                    <h2 id="sugerencia-de-productos-POSTapi-v1-farmers-suggested_products">Almacena un nuevo producto sugerido.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-farmers-suggested_products">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/suggested_products" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/suggested_products"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-suggested_products">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del producto sugerido&quot;,
        &quot;user_id&quot;: &quot;ID del usuario que sugiri&oacute; el producto&quot;,
        &quot;name&quot;: &quot;Nombre del producto sugerido&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n del producto sugerido&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del producto sugerido&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del producto sugerido&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurrri&oacute; un error al almacenar el producto sugerido.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-suggested_products" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-suggested_products"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-suggested_products"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-suggested_products" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-suggested_products">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-suggested_products" data-method="POST"
      data-path="api/v1/farmers/suggested_products"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-suggested_products', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-suggested_products"
                    onclick="tryItOut('POSTapi-v1-farmers-suggested_products');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-suggested_products"
                    onclick="cancelTryOut('POSTapi-v1-farmers-suggested_products');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-suggested_products"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/suggested_products</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-suggested_products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-suggested_products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="sugerencia-de-productos-POSTapi-v1-farmers-suggested_products--id-">Actualiza el producto sugerido especificado en el almacenamiento.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-farmers-suggested_products--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/farmers/suggested_products/numquam" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/suggested_products/numquam"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-farmers-suggested_products--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del producto sugerido&quot;,
        &quot;user_id&quot;: &quot;ID del usuario que sugiri&oacute; el producto&quot;,
        &quot;name&quot;: &quot;Nombre del producto sugerido&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n del producto sugerido&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del producto sugerido&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del producto sugerido&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-farmers-suggested_products--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-farmers-suggested_products--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-farmers-suggested_products--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-farmers-suggested_products--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-farmers-suggested_products--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-farmers-suggested_products--id-" data-method="POST"
      data-path="api/v1/farmers/suggested_products/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-farmers-suggested_products--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-farmers-suggested_products--id-"
                    onclick="tryItOut('POSTapi-v1-farmers-suggested_products--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-farmers-suggested_products--id-"
                    onclick="cancelTryOut('POSTapi-v1-farmers-suggested_products--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-farmers-suggested_products--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/farmers/suggested_products/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-farmers-suggested_products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-farmers-suggested_products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-farmers-suggested_products--id-"
               value="numquam"
               data-component="url">
    <br>
<p>The ID of the suggested product. Example: <code>numquam</code></p>
            </div>
                    </form>

                    <h2 id="sugerencia-de-productos-DELETEapi-v1-farmers-suggested_products--id-">Elimina el producto sugerido especificado del almacenamiento.</h2>

<p>
</p>



<span id="example-requests-DELETEapi-v1-farmers-suggested_products--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request DELETE \
    "http://agrimarket-api.test/api/v1/farmers/suggested_products/molestiae" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/farmers/suggested_products/molestiae"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-DELETEapi-v1-farmers-suggested_products--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: &quot;Sugerencia eliminada&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-DELETEapi-v1-farmers-suggested_products--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-DELETEapi-v1-farmers-suggested_products--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-v1-farmers-suggested_products--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-DELETEapi-v1-farmers-suggested_products--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-v1-farmers-suggested_products--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-DELETEapi-v1-farmers-suggested_products--id-" data-method="DELETE"
      data-path="api/v1/farmers/suggested_products/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('DELETEapi-v1-farmers-suggested_products--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-DELETEapi-v1-farmers-suggested_products--id-"
                    onclick="tryItOut('DELETEapi-v1-farmers-suggested_products--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-DELETEapi-v1-farmers-suggested_products--id-"
                    onclick="cancelTryOut('DELETEapi-v1-farmers-suggested_products--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-DELETEapi-v1-farmers-suggested_products--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-red">DELETE</small>
            <b><code>api/v1/farmers/suggested_products/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="DELETEapi-v1-farmers-suggested_products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="DELETEapi-v1-farmers-suggested_products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="DELETEapi-v1-farmers-suggested_products--id-"
               value="molestiae"
               data-component="url">
    <br>
<p>The ID of the suggested product. Example: <code>molestiae</code></p>
            </div>
                    </form>

                    <h2 id="sugerencia-de-productos-GETapi-v1-suggested_products">Muestra una lista de todos los productos sugeridos.</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-suggested_products">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/suggested_products" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/suggested_products"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-suggested_products">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: &quot;ID del producto sugerido&quot;,
            &quot;user_id&quot;: &quot;ID del usuario que sugiri&oacute; el producto&quot;,
            &quot;name&quot;: &quot;Nombre del producto sugerido&quot;,
            &quot;description&quot;: &quot;Descripci&oacute;n del producto sugerido&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del producto sugerido&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del producto sugerido&quot;
        },
        ...
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-suggested_products" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-suggested_products"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-suggested_products"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-suggested_products" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-suggested_products">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-suggested_products" data-method="GET"
      data-path="api/v1/suggested_products"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-suggested_products', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-suggested_products"
                    onclick="tryItOut('GETapi-v1-suggested_products');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-suggested_products"
                    onclick="cancelTryOut('GETapi-v1-suggested_products');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-suggested_products"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/suggested_products</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-suggested_products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-suggested_products"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="sugerencia-de-productos-GETapi-v1-suggested_products--id-">Muestra el producto sugerido especificado.</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-suggested_products--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/suggested_products/et" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/suggested_products/et"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-suggested_products--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del producto sugerido&quot;,
        &quot;user_id&quot;: &quot;ID del usuario que sugiri&oacute; el producto&quot;,
        &quot;name&quot;: &quot;Nombre del producto sugerido&quot;,
        &quot;description&quot;: &quot;Descripci&oacute;n del producto sugerido&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del producto sugerido&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del producto sugerido&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-suggested_products--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-suggested_products--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-suggested_products--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-suggested_products--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-suggested_products--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-suggested_products--id-" data-method="GET"
      data-path="api/v1/suggested_products/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-suggested_products--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-suggested_products--id-"
                    onclick="tryItOut('GETapi-v1-suggested_products--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-suggested_products--id-"
                    onclick="cancelTryOut('GETapi-v1-suggested_products--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-suggested_products--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/suggested_products/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-suggested_products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-suggested_products--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-suggested_products--id-"
               value="et"
               data-component="url">
    <br>
<p>The ID of the suggested product. Example: <code>et</code></p>
            </div>
                    </form>

                <h1 id="tipos-de-productos">Tipos de Productos</h1>

    

                                <h2 id="tipos-de-productos-POSTapi-v1-admins-find_product_type">Busca tipos de productos por nombre.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite buscar tipos de productos por su nombre.</p>

<span id="example-requests-POSTapi-v1-admins-find_product_type">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/find_product_type?name=necessitatibus" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/find_product_type"
);

const params = {
    "name": "necessitatibus",
};
Object.keys(params)
    .forEach(key =&gt; url.searchParams.append(key, params[key]));

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-find_product_type">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;name&quot;: &quot;Nombre del tipo de producto&quot;,
            &quot;category&quot;: &quot;Nombre de la categor&iacute;a&quot;,
            &quot;active&quot;: true,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        },
        {
            ...
        }
    ]
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;No se encontr&oacute; ning&uacute;n registro&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-find_product_type" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-find_product_type"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-find_product_type"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-find_product_type" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-find_product_type">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-find_product_type" data-method="POST"
      data-path="api/v1/admins/find_product_type"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-find_product_type', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-find_product_type"
                    onclick="tryItOut('POSTapi-v1-admins-find_product_type');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-find_product_type"
                    onclick="cancelTryOut('POSTapi-v1-admins-find_product_type');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-find_product_type"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/find_product_type</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-find_product_type"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-find_product_type"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                            <h4 class="fancy-heading-panel"><b>Query Parameters</b></h4>
                                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>name</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="name"                data-endpoint="POSTapi-v1-admins-find_product_type"
               value="necessitatibus"
               data-component="query">
    <br>
<p>El nombre del tipo de producto a buscar. Example: <code>necessitatibus</code></p>
            </div>
                </form>

                    <h2 id="tipos-de-productos-POSTapi-v1-admins-product_types">Almacena un nuevo tipo de producto.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite a un usuario almacenar un nuevo tipo de producto en el sistema.</p>

<span id="example-requests-POSTapi-v1-admins-product_types">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/product_types" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"name\": \"facilis\",
    \"category_id\": 16
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/product_types"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "facilis",
    "category_id": 16
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-product_types">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre del tipo de producto&quot;,
        &quot;category&quot;: &quot;Nombre de la categor&iacute;a&quot;,
        &quot;active&quot;: true,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurrrio un error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-product_types" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-product_types"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-product_types"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-product_types" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-product_types">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-product_types" data-method="POST"
      data-path="api/v1/admins/product_types"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-product_types', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-product_types"
                    onclick="tryItOut('POSTapi-v1-admins-product_types');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-product_types"
                    onclick="cancelTryOut('POSTapi-v1-admins-product_types');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-product_types"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/product_types</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-product_types"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-product_types"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>name</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="name"                data-endpoint="POSTapi-v1-admins-product_types"
               value="facilis"
               data-component="body">
    <br>
<p>El nombre del tipo de producto. Example: <code>facilis</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>category_id</code></b>&nbsp;&nbsp;
<small>integer</small>&nbsp;
 &nbsp;
                <input type="number" style="display: none"
               step="any"               name="category_id"                data-endpoint="POSTapi-v1-admins-product_types"
               value="16"
               data-component="body">
    <br>
<p>El ID de la categoría del tipo de producto. Example: <code>16</code></p>
        </div>
        </form>

                    <h2 id="tipos-de-productos-GETapi-v1-admins-product_types--id-">Muestra un tipo de producto específico.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve información detallada sobre un tipo de producto específico.</p>

<span id="example-requests-GETapi-v1-admins-product_types--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/admins/product_types/illum" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/product_types/illum"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-admins-product_types--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre del tipo de producto&quot;,
        &quot;category&quot;: &quot;Nombre de la categor&iacute;a&quot;,
        &quot;active&quot;: true,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-admins-product_types--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-admins-product_types--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-admins-product_types--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-admins-product_types--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-admins-product_types--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-admins-product_types--id-" data-method="GET"
      data-path="api/v1/admins/product_types/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-admins-product_types--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-admins-product_types--id-"
                    onclick="tryItOut('GETapi-v1-admins-product_types--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-admins-product_types--id-"
                    onclick="cancelTryOut('GETapi-v1-admins-product_types--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-admins-product_types--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/admins/product_types/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-admins-product_types--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-admins-product_types--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-admins-product_types--id-"
               value="illum"
               data-component="url">
    <br>
<p>El ID del tipo de producto. Example: <code>illum</code></p>
            </div>
                    </form>

                    <h2 id="tipos-de-productos-POSTapi-v1-admins-product_types--id-">Actualiza un tipo de producto existente.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite actualizar la información de un tipo de producto existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-admins-product_types--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/product_types/incidunt" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/product_types/incidunt"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-product_types--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre del tipo de producto&quot;,
        &quot;category&quot;: &quot;Nombre de la categor&iacute;a&quot;,
        &quot;active&quot;: true,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-product_types--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-product_types--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-product_types--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-product_types--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-product_types--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-product_types--id-" data-method="POST"
      data-path="api/v1/admins/product_types/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-product_types--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-product_types--id-"
                    onclick="tryItOut('POSTapi-v1-admins-product_types--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-product_types--id-"
                    onclick="cancelTryOut('POSTapi-v1-admins-product_types--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-product_types--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/product_types/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-product_types--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-product_types--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-product_types--id-"
               value="incidunt"
               data-component="url">
    <br>
<p>El ID del tipo de producto. Example: <code>incidunt</code></p>
            </div>
                    </form>

                    <h2 id="tipos-de-productos-POSTapi-v1-admins-product_types--id--active">Elimina un tipo de producto existente.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite eliminar un tipo de producto existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-admins-product_types--id--active">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/product_types/non/active" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/product_types/non/active"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-product_types--id--active">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;name&quot;: &quot;Nombre del tipo de producto&quot;,
        &quot;category&quot;: &quot;Nombre de la categor&iacute;a&quot;,
        &quot;active&quot;: true,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-product_types--id--active" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-product_types--id--active"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-product_types--id--active"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-product_types--id--active" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-product_types--id--active">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-product_types--id--active" data-method="POST"
      data-path="api/v1/admins/product_types/{id}/active"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-product_types--id--active', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-product_types--id--active"
                    onclick="tryItOut('POSTapi-v1-admins-product_types--id--active');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-product_types--id--active"
                    onclick="cancelTryOut('POSTapi-v1-admins-product_types--id--active');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-product_types--id--active"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/product_types/{id}/active</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-product_types--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-product_types--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-product_types--id--active"
               value="non"
               data-component="url">
    <br>
<p>El ID del tipo de producto. Example: <code>non</code></p>
            </div>
                    </form>

                    <h2 id="tipos-de-productos-GETapi-v1-product_types">Muestra una lista de tipos de productos.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve una lista de tipos de productos disponibles en el sistema.</p>

<span id="example-requests-GETapi-v1-product_types">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/product_types" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/product_types"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-product_types">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;name&quot;: &quot;Nombre del tipo de producto&quot;,
            &quot;category&quot;: &quot;Nombre de la categor&iacute;a&quot;,
            &quot;active&quot;: true,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        },
        {
            ...
        }
    ]
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Tu usuario no cuenta con un rol indicado&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-product_types" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-product_types"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-product_types"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-product_types" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-product_types">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-product_types" data-method="GET"
      data-path="api/v1/product_types"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-product_types', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-product_types"
                    onclick="tryItOut('GETapi-v1-product_types');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-product_types"
                    onclick="cancelTryOut('GETapi-v1-product_types');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-product_types"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/product_types</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-product_types"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-product_types"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                <h1 id="unidades-de-medida">Unidades de medida</h1>

    

                                <h2 id="unidades-de-medida-POSTapi-v1-admins-units_of_measurements">Almacena una nueva unidad de medida.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-admins-units_of_measurements">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/units_of_measurements" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/units_of_measurements"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-units_of_measurements">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID de la unidad de medida&quot;,
        &quot;name&quot;: &quot;Nombre de la unidad de medida&quot;,
        &quot;code&quot;: &quot;C&oacute;digo de la unidad de medida&quot;,
        &quot;active&quot;: &quot;Estado de la unidad de medida (activo o inactivo)&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n de la unidad de medida&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n de la unidad de medida&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurri&oacute; un error al almacenar la unidad de medida.&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-units_of_measurements" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-units_of_measurements"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-units_of_measurements"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-units_of_measurements" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-units_of_measurements">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-units_of_measurements" data-method="POST"
      data-path="api/v1/admins/units_of_measurements"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-units_of_measurements', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-units_of_measurements"
                    onclick="tryItOut('POSTapi-v1-admins-units_of_measurements');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-units_of_measurements"
                    onclick="cancelTryOut('POSTapi-v1-admins-units_of_measurements');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-units_of_measurements"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/units_of_measurements</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-units_of_measurements"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-units_of_measurements"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="unidades-de-medida-GETapi-v1-admins-units_of_measurements--id-">Muestra la unidad de medida especificada.</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-admins-units_of_measurements--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/admins/units_of_measurements/eveniet" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/units_of_measurements/eveniet"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-admins-units_of_measurements--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID de la unidad de medida&quot;,
        &quot;name&quot;: &quot;Nombre de la unidad de medida&quot;,
        &quot;code&quot;: &quot;C&oacute;digo de la unidad de medida&quot;,
        &quot;active&quot;: &quot;Estado de la unidad de medida (activo o inactivo)&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n de la unidad de medida&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n de la unidad de medida&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-admins-units_of_measurements--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-admins-units_of_measurements--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-admins-units_of_measurements--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-admins-units_of_measurements--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-admins-units_of_measurements--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-admins-units_of_measurements--id-" data-method="GET"
      data-path="api/v1/admins/units_of_measurements/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-admins-units_of_measurements--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-admins-units_of_measurements--id-"
                    onclick="tryItOut('GETapi-v1-admins-units_of_measurements--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-admins-units_of_measurements--id-"
                    onclick="cancelTryOut('GETapi-v1-admins-units_of_measurements--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-admins-units_of_measurements--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/admins/units_of_measurements/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-admins-units_of_measurements--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-admins-units_of_measurements--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-admins-units_of_measurements--id-"
               value="eveniet"
               data-component="url">
    <br>
<p>The ID of the units of measurement. Example: <code>eveniet</code></p>
            </div>
                    </form>

                    <h2 id="unidades-de-medida-POSTapi-v1-admins-units_of_measurements--id-">Actualiza la unidad de medida especificada en el almacenamiento.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-admins-units_of_measurements--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/units_of_measurements/ut" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/units_of_measurements/ut"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-units_of_measurements--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID de la unidad de medida&quot;,
        &quot;name&quot;: &quot;Nombre de la unidad de medida&quot;,
        &quot;code&quot;: &quot;C&oacute;digo de la unidad de medida&quot;,
        &quot;active&quot;: &quot;Estado de la unidad de medida (activo o inactivo)&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n de la unidad de medida&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n de la unidad de medida&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-units_of_measurements--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-units_of_measurements--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-units_of_measurements--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-units_of_measurements--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-units_of_measurements--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-units_of_measurements--id-" data-method="POST"
      data-path="api/v1/admins/units_of_measurements/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-units_of_measurements--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-units_of_measurements--id-"
                    onclick="tryItOut('POSTapi-v1-admins-units_of_measurements--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-units_of_measurements--id-"
                    onclick="cancelTryOut('POSTapi-v1-admins-units_of_measurements--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-units_of_measurements--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/units_of_measurements/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-units_of_measurements--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-units_of_measurements--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-units_of_measurements--id-"
               value="ut"
               data-component="url">
    <br>
<p>The ID of the units of measurement. Example: <code>ut</code></p>
            </div>
                    </form>

                    <h2 id="unidades-de-medida-POSTapi-v1-admins-units_of_measurements--id--active">Elimina la unidad de medida especificada del almacenamiento.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-admins-units_of_measurements--id--active">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/units_of_measurements/necessitatibus/active" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/units_of_measurements/necessitatibus/active"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-units_of_measurements--id--active">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: &quot;Unidad de medida eliminada&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-units_of_measurements--id--active" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-units_of_measurements--id--active"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-units_of_measurements--id--active"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-units_of_measurements--id--active" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-units_of_measurements--id--active">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-units_of_measurements--id--active" data-method="POST"
      data-path="api/v1/admins/units_of_measurements/{id}/active"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-units_of_measurements--id--active', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-units_of_measurements--id--active"
                    onclick="tryItOut('POSTapi-v1-admins-units_of_measurements--id--active');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-units_of_measurements--id--active"
                    onclick="cancelTryOut('POSTapi-v1-admins-units_of_measurements--id--active');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-units_of_measurements--id--active"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/units_of_measurements/{id}/active</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-units_of_measurements--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-units_of_measurements--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-admins-units_of_measurements--id--active"
               value="necessitatibus"
               data-component="url">
    <br>
<p>The ID of the units of measurement. Example: <code>necessitatibus</code></p>
            </div>
                    </form>

                    <h2 id="unidades-de-medida-GETapi-v1-units_of_measurements">Muestra una lista de todas las unidades de medida.</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-units_of_measurements">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/units_of_measurements" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/units_of_measurements"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-units_of_measurements">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: &quot;ID de la unidad de medida&quot;,
            &quot;name&quot;: &quot;Nombre de la unidad de medida&quot;,
            &quot;code&quot;: &quot;C&oacute;digo de la unidad de medida&quot;,
            &quot;active&quot;: &quot;Estado de la unidad de medida (activo o inactivo)&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n de la unidad de medida&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n de la unidad de medida&quot;
        },
        ...
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-units_of_measurements" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-units_of_measurements"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-units_of_measurements"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-units_of_measurements" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-units_of_measurements">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-units_of_measurements" data-method="GET"
      data-path="api/v1/units_of_measurements"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-units_of_measurements', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-units_of_measurements"
                    onclick="tryItOut('GETapi-v1-units_of_measurements');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-units_of_measurements"
                    onclick="cancelTryOut('GETapi-v1-units_of_measurements');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-units_of_measurements"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/units_of_measurements</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-units_of_measurements"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-units_of_measurements"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                <h1 id="usuarios">Usuarios</h1>

    

                                <h2 id="usuarios-GETapi-v1-admins-users">Muestra una lista de todos los usuarios.</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-admins-users">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/admins/users" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/users"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-admins-users">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: &quot;ID del usuario&quot;,
            &quot;first_name&quot;: &quot;Nombre del usuario&quot;,
            &quot;last_name&quot;: &quot;Apellido del usuario&quot;,
            &quot;email&quot;: &quot;Correo electr&oacute;nico del usuario&quot;,
            &quot;role&quot;: &quot;Rol del usuario&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del usuario&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del usuario&quot;
        },
        ...
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-admins-users" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-admins-users"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-admins-users"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-admins-users" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-admins-users">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-admins-users" data-method="GET"
      data-path="api/v1/admins/users"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-admins-users', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-admins-users"
                    onclick="tryItOut('GETapi-v1-admins-users');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-admins-users"
                    onclick="cancelTryOut('GETapi-v1-admins-users');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-admins-users"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/admins/users</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-admins-users"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-admins-users"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="usuarios-POSTapi-v1-admins-find_user">Busca usuarios por nombre, apellido o correo electrónico.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-admins-find_user">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/admins/find_user" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/admins/find_user"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-admins-find_user">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: &quot;ID del usuario&quot;,
            &quot;first_name&quot;: &quot;Nombre del usuario&quot;,
            &quot;last_name&quot;: &quot;Apellido del usuario&quot;,
            &quot;email&quot;: &quot;Correo electr&oacute;nico del usuario&quot;,
            &quot;role&quot;: &quot;Rol del usuario&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del usuario&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del usuario&quot;
        },
        ...
    ]
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;No se encontr&oacute; un nombre, apellido o e-mail&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-admins-find_user" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-admins-find_user"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-admins-find_user"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-admins-find_user" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-admins-find_user">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-admins-find_user" data-method="POST"
      data-path="api/v1/admins/find_user"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-admins-find_user', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-admins-find_user"
                    onclick="tryItOut('POSTapi-v1-admins-find_user');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-admins-find_user"
                    onclick="cancelTryOut('POSTapi-v1-admins-find_user');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-admins-find_user"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/admins/find_user</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-admins-find_user"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-admins-find_user"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="usuarios-GETapi-v1-users-me">Muestra el usuario autenticado.</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-users-me">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/users/me" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/users/me"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-users-me">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del usuario&quot;,
        &quot;first_name&quot;: &quot;Nombre del usuario&quot;,
        &quot;last_name&quot;: &quot;Apellido del usuario&quot;,
        &quot;email&quot;: &quot;Correo electr&oacute;nico del usuario&quot;,
        &quot;phone&quot;: &quot;N&uacute;mero de tel&eacute;fono del usuario&quot;,
        &quot;street&quot;: &quot;Calle del usuario&quot;,
        &quot;ext_num&quot;: &quot;N&uacute;mero exterior del usuario&quot;,
        &quot;int_num&quot;: &quot;N&uacute;mero interior del usuario&quot;,
        &quot;suburb&quot;: &quot;Colonia del usuario&quot;,
        &quot;city&quot;: &quot;Ciudad del usuario&quot;,
        &quot;state&quot;: &quot;Estado del usuario&quot;,
        &quot;zip_code&quot;: &quot;C&oacute;digo postal del usuario&quot;,
        &quot;photo&quot;: &quot;URL de la foto del usuario&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del usuario&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del usuario&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-users-me" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-users-me"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-users-me"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-users-me" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-users-me">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-users-me" data-method="GET"
      data-path="api/v1/users/me"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-users-me', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-users-me"
                    onclick="tryItOut('GETapi-v1-users-me');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-users-me"
                    onclick="cancelTryOut('GETapi-v1-users-me');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-users-me"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/users/me</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-users-me"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-users-me"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="usuarios-POSTapi-v1-users-push_id">Almacena la push ID del usuario autenticado.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-users-push_id">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/users/push_id" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/users/push_id"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-users-push_id">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: &quot;Push ID actualizada&quot;
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;No se recibi&oacute; la push ID&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-users-push_id" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-users-push_id"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-users-push_id"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-users-push_id" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-users-push_id">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-users-push_id" data-method="POST"
      data-path="api/v1/users/push_id"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-users-push_id', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-users-push_id"
                    onclick="tryItOut('POSTapi-v1-users-push_id');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-users-push_id"
                    onclick="cancelTryOut('POSTapi-v1-users-push_id');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-users-push_id"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/users/push_id</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-users-push_id"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-users-push_id"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="usuarios-POSTapi-v1-users-me-update">Actualiza los datos del usuario autenticado.</h2>

<p>
</p>



<span id="example-requests-POSTapi-v1-users-me-update">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/users/me/update" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/users/me/update"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-users-me-update">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del usuario&quot;,
        &quot;first_name&quot;: &quot;Nombre del usuario&quot;,
        &quot;last_name&quot;: &quot;Apellido del usuario&quot;,
        &quot;email&quot;: &quot;Correo electr&oacute;nico del usuario&quot;,
        &quot;phone&quot;: &quot;N&uacute;mero de tel&eacute;fono del usuario&quot;,
        &quot;street&quot;: &quot;Calle del usuario&quot;,
        &quot;ext_num&quot;: &quot;N&uacute;mero exterior del usuario&quot;,
        &quot;int_num&quot;: &quot;N&uacute;mero interior del usuario&quot;,
        &quot;suburb&quot;: &quot;Colonia del usuario&quot;,
        &quot;city&quot;: &quot;Ciudad del usuario&quot;,
        &quot;state&quot;: &quot;Estado del usuario&quot;,
        &quot;zip_code&quot;: &quot;C&oacute;digo postal del usuario&quot;,
        &quot;photo&quot;: &quot;URL de la foto del usuario&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del usuario&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del usuario&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-users-me-update" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-users-me-update"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-users-me-update"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-users-me-update" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-users-me-update">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-users-me-update" data-method="POST"
      data-path="api/v1/users/me/update"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-users-me-update', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-users-me-update"
                    onclick="tryItOut('POSTapi-v1-users-me-update');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-users-me-update"
                    onclick="cancelTryOut('POSTapi-v1-users-me-update');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-users-me-update"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/users/me/update</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-users-me-update"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-users-me-update"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="usuarios-GETapi-v1-users--id-">Muestra el usuario especificado.</h2>

<p>
</p>



<span id="example-requests-GETapi-v1-users--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/users/ut" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/users/ut"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-users--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: &quot;ID del usuario&quot;,
        &quot;first_name&quot;: &quot;Nombre del usuario&quot;,
        &quot;last_name&quot;: &quot;Apellido del usuario&quot;,
        &quot;email&quot;: &quot;Correo electr&oacute;nico del usuario&quot;,
        &quot;phone&quot;: &quot;N&uacute;mero de tel&eacute;fono del usuario&quot;,
        &quot;street&quot;: &quot;Calle del usuario&quot;,
        &quot;ext_num&quot;: &quot;N&uacute;mero exterior del usuario&quot;,
        &quot;int_num&quot;: &quot;N&uacute;mero interior del usuario&quot;,
        &quot;suburb&quot;: &quot;Colonia del usuario&quot;,
        &quot;city&quot;: &quot;Ciudad del usuario&quot;,
        &quot;state&quot;: &quot;Estado del usuario&quot;,
        &quot;zip_code&quot;: &quot;C&oacute;digo postal del usuario&quot;,
        &quot;photo&quot;: &quot;URL de la foto del usuario&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n del usuario&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n del usuario&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-users--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-users--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-users--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-users--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-users--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-users--id-" data-method="GET"
      data-path="api/v1/users/{id}"
      data-authed="0"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-users--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-users--id-"
                    onclick="tryItOut('GETapi-v1-users--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-users--id-"
                    onclick="cancelTryOut('GETapi-v1-users--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-users--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/users/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-users--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-users--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-users--id-"
               value="ut"
               data-component="url">
    <br>
<p>The ID of the user. Example: <code>ut</code></p>
            </div>
                    </form>

                <h1 id="ordenes">Órdenes</h1>

    

                                <h2 id="ordenes-POSTapi-v1-clients-orders">Almacena una nueva orden.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite almacenar una nueva orden de compra en el sistema.</p>

<span id="example-requests-POSTapi-v1-clients-orders">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/clients/orders" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"product_id\": 10,
    \"quantity\": 8
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/clients/orders"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "product_id": 10,
    "quantity": 8
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-clients-orders">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;product_id&quot;: 1,
        &quot;quantity&quot;: 10,
        &quot;total&quot;: 100,
        &quot;status&quot;: &quot;Pendiente&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (400):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Ocurrrio un error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-clients-orders" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-clients-orders"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-clients-orders"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-clients-orders" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-clients-orders">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-clients-orders" data-method="POST"
      data-path="api/v1/clients/orders"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-clients-orders', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-clients-orders"
                    onclick="tryItOut('POSTapi-v1-clients-orders');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-clients-orders"
                    onclick="cancelTryOut('POSTapi-v1-clients-orders');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-clients-orders"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/clients/orders</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-clients-orders"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-clients-orders"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>product_id</code></b>&nbsp;&nbsp;
<small>integer</small>&nbsp;
 &nbsp;
                <input type="number" style="display: none"
               step="any"               name="product_id"                data-endpoint="POSTapi-v1-clients-orders"
               value="10"
               data-component="body">
    <br>
<p>El ID del producto. Example: <code>10</code></p>
        </div>
                <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>quantity</code></b>&nbsp;&nbsp;
<small>integer</small>&nbsp;
 &nbsp;
                <input type="number" style="display: none"
               step="any"               name="quantity"                data-endpoint="POSTapi-v1-clients-orders"
               value="8"
               data-component="body">
    <br>
<p>La cantidad del producto. Example: <code>8</code></p>
        </div>
        </form>

                    <h2 id="ordenes-POSTapi-v1-clients-orders--id-">Actualiza una orden existente.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite actualizar una orden existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-clients-orders--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/clients/orders/dolores" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/clients/orders/dolores"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-clients-orders--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;product_id&quot;: 1,
        &quot;quantity&quot;: 20,
        &quot;total&quot;: 200,
        &quot;status&quot;: &quot;Pendiente&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-clients-orders--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-clients-orders--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-clients-orders--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-clients-orders--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-clients-orders--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-clients-orders--id-" data-method="POST"
      data-path="api/v1/clients/orders/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-clients-orders--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-clients-orders--id-"
                    onclick="tryItOut('POSTapi-v1-clients-orders--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-clients-orders--id-"
                    onclick="cancelTryOut('POSTapi-v1-clients-orders--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-clients-orders--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/clients/orders/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-clients-orders--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-clients-orders--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-clients-orders--id-"
               value="dolores"
               data-component="url">
    <br>
<p>El ID de la orden. Example: <code>dolores</code></p>
            </div>
                    </form>

                    <h2 id="ordenes-POSTapi-v1-clients-orders--id--active">Elimina una orden.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite eliminar una orden existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-clients-orders--id--active">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/clients/orders/est/active" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/clients/orders/est/active"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-clients-orders--id--active">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;product_id&quot;: 1,
        &quot;quantity&quot;: 10,
        &quot;total&quot;: 100,
        &quot;status&quot;: &quot;Pendiente&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-clients-orders--id--active" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-clients-orders--id--active"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-clients-orders--id--active"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-clients-orders--id--active" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-clients-orders--id--active">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-clients-orders--id--active" data-method="POST"
      data-path="api/v1/clients/orders/{id}/active"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-clients-orders--id--active', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-clients-orders--id--active"
                    onclick="tryItOut('POSTapi-v1-clients-orders--id--active');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-clients-orders--id--active"
                    onclick="cancelTryOut('POSTapi-v1-clients-orders--id--active');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-clients-orders--id--active"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/clients/orders/{id}/active</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-clients-orders--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-clients-orders--id--active"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-clients-orders--id--active"
               value="est"
               data-component="url">
    <br>
<p>El ID de la orden. Example: <code>est</code></p>
            </div>
                    </form>

                    <h2 id="ordenes-GETapi-v1-orders">Obtiene una lista de órdenes.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint devuelve una lista de órdenes de compra, filtradas según el rol del usuario autenticado.</p>

<span id="example-requests-GETapi-v1-orders">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/orders" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/orders"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-orders">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: [
        {
            &quot;id&quot;: 1,
            &quot;product_id&quot;: 1,
            &quot;quantity&quot;: 10,
            &quot;total&quot;: 100,
            &quot;status&quot;: &quot;Pendiente&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        },
        {
            &quot;id&quot;: 2,
            &quot;product_id&quot;: 2,
            &quot;quantity&quot;: 20,
            &quot;total&quot;: 200,
            &quot;status&quot;: &quot;Pendiente&quot;,
            &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
            &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
        }
    ]
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-orders" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-orders"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-orders"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-orders" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-orders">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-orders" data-method="GET"
      data-path="api/v1/orders"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-orders', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-orders"
                    onclick="tryItOut('GETapi-v1-orders');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-orders"
                    onclick="cancelTryOut('GETapi-v1-orders');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-orders"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/orders</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-orders"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-orders"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        </form>

                    <h2 id="ordenes-GETapi-v1-orders--id-">Muestra una orden específica.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint muestra detalles de una orden específica según el ID proporcionado.</p>

<span id="example-requests-GETapi-v1-orders--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request GET \
    --get "http://agrimarket-api.test/api/v1/orders/sit" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json"</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/orders/sit"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers,
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-GETapi-v1-orders--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;product_id&quot;: 1,
        &quot;quantity&quot;: 10,
        &quot;total&quot;: 100,
        &quot;status&quot;: &quot;Pendiente&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
    </span>
<span id="execution-results-GETapi-v1-orders--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-GETapi-v1-orders--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-v1-orders--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-GETapi-v1-orders--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-v1-orders--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-GETapi-v1-orders--id-" data-method="GET"
      data-path="api/v1/orders/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('GETapi-v1-orders--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-GETapi-v1-orders--id-"
                    onclick="tryItOut('GETapi-v1-orders--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-GETapi-v1-orders--id-"
                    onclick="cancelTryOut('GETapi-v1-orders--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-GETapi-v1-orders--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-green">GET</small>
            <b><code>api/v1/orders/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="GETapi-v1-orders--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="GETapi-v1-orders--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="GETapi-v1-orders--id-"
               value="sit"
               data-component="url">
    <br>
<p>El ID de la orden. Example: <code>sit</code></p>
            </div>
                    </form>

                    <h2 id="ordenes-POSTapi-v1-orders-update_order_status--id-">Actualiza el estado de una orden.</h2>

<p>
<small class="badge badge-darkred">requires authentication</small>
</p>

<p>Este endpoint permite actualizar el estado de una orden existente en el sistema.</p>

<span id="example-requests-POSTapi-v1-orders-update_order_status--id-">
<blockquote>Example request:</blockquote>


<div class="bash-example">
    <pre><code class="language-bash">curl --request POST \
    "http://agrimarket-api.test/api/v1/orders/update_order_status/quos" \
    --header "Content-Type: application/json" \
    --header "Accept: application/json" \
    --data "{
    \"status\": \"corrupti\"
}"
</code></pre></div>


<div class="javascript-example">
    <pre><code class="language-javascript">const url = new URL(
    "http://agrimarket-api.test/api/v1/orders/update_order_status/quos"
);

const headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "status": "corrupti"
};

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response =&gt; response.json());</code></pre></div>

</span>

<span id="example-responses-POSTapi-v1-orders-update_order_status--id-">
            <blockquote>
            <p>Example response (200):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;data&quot;: {
        &quot;id&quot;: 1,
        &quot;product_id&quot;: 1,
        &quot;quantity&quot;: 10,
        &quot;total&quot;: 100,
        &quot;status&quot;: &quot;Completado&quot;,
        &quot;created_at&quot;: &quot;Fecha de creaci&oacute;n&quot;,
        &quot;updated_at&quot;: &quot;Fecha de actualizaci&oacute;n&quot;
    }
}</code>
 </pre>
            <blockquote>
            <p>Example response (500):</p>
        </blockquote>
                <pre>

<code class="language-json" style="max-height: 300px;">{
    &quot;error&quot;: &quot;Descripci&oacute;n del error&quot;
}</code>
 </pre>
    </span>
<span id="execution-results-POSTapi-v1-orders-update_order_status--id-" hidden>
    <blockquote>Received response<span
                id="execution-response-status-POSTapi-v1-orders-update_order_status--id-"></span>:
    </blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-v1-orders-update_order_status--id-"
      data-empty-response-text="<Empty response>" style="max-height: 400px;"></code></pre>
</span>
<span id="execution-error-POSTapi-v1-orders-update_order_status--id-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-v1-orders-update_order_status--id-">

Tip: Check that you&#039;re properly connected to the network.
If you&#039;re a maintainer of ths API, verify that your API is running and you&#039;ve enabled CORS.
You can check the Dev Tools console for debugging information.</code></pre>
</span>
<form id="form-POSTapi-v1-orders-update_order_status--id-" data-method="POST"
      data-path="api/v1/orders/update_order_status/{id}"
      data-authed="1"
      data-hasfiles="0"
      data-isarraybody="0"
      autocomplete="off"
      onsubmit="event.preventDefault(); executeTryOut('POSTapi-v1-orders-update_order_status--id-', this);">
    <h3>
        Request&nbsp;&nbsp;&nbsp;
                    <button type="button"
                    style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-tryout-POSTapi-v1-orders-update_order_status--id-"
                    onclick="tryItOut('POSTapi-v1-orders-update_order_status--id-');">Try it out ⚡
            </button>
            <button type="button"
                    style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-canceltryout-POSTapi-v1-orders-update_order_status--id-"
                    onclick="cancelTryOut('POSTapi-v1-orders-update_order_status--id-');" hidden>Cancel 🛑
            </button>&nbsp;&nbsp;
            <button type="submit"
                    style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;"
                    id="btn-executetryout-POSTapi-v1-orders-update_order_status--id-"
                    data-initial-text="Send Request 💥"
                    data-loading-text="⏱ Sending..."
                    hidden>Send Request 💥
            </button>
            </h3>
            <p>
            <small class="badge badge-black">POST</small>
            <b><code>api/v1/orders/update_order_status/{id}</code></b>
        </p>
                <h4 class="fancy-heading-panel"><b>Headers</b></h4>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Content-Type</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Content-Type"                data-endpoint="POSTapi-v1-orders-update_order_status--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                                <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>Accept</code></b>&nbsp;&nbsp;
&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="Accept"                data-endpoint="POSTapi-v1-orders-update_order_status--id-"
               value="application/json"
               data-component="header">
    <br>
<p>Example: <code>application/json</code></p>
            </div>
                        <h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
                    <div style="padding-left: 28px; clear: unset;">
                <b style="line-height: 2;"><code>id</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="id"                data-endpoint="POSTapi-v1-orders-update_order_status--id-"
               value="quos"
               data-component="url">
    <br>
<p>El ID de la orden. Example: <code>quos</code></p>
            </div>
                            <h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
        <div style=" padding-left: 28px;  clear: unset;">
            <b style="line-height: 2;"><code>status</code></b>&nbsp;&nbsp;
<small>string</small>&nbsp;
 &nbsp;
                <input type="text" style="display: none"
                              name="status"                data-endpoint="POSTapi-v1-orders-update_order_status--id-"
               value="corrupti"
               data-component="body">
    <br>
<p>El nuevo estado de la orden. Example: <code>corrupti</code></p>
        </div>
        </form>

            

        
    </div>
    <div class="dark-box">
                    <div class="lang-selector">
                                                        <button type="button" class="lang-button" data-language-name="bash">bash</button>
                                                        <button type="button" class="lang-button" data-language-name="javascript">javascript</button>
                            </div>
            </div>
</div>
</body>
</html>
