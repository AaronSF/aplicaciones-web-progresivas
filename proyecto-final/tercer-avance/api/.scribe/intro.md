# Introduction



<aside>
    <strong>Base URL</strong>: <code>http://agrimarket-api.test</code>
</aside>

Bienvenido a la Agrimarket API, una plataforma diseñada para ayudar a gestionar eficientemente los diferentes aspectos relacionados con la agricultura. Esta API proporciona acceso a una variedad de recursos que abarcan desde la administración de usuarios hasta la gestión de productos y propiedades inmobiliarias.

<aside>
 <p style="font-size: 30px; font-weight: 700;">Recursos Disponibles</p>
    <p style="font-size: 20px; font-weight: 700;">Administradores</p>
    <p>
        El recurso de Administradores permite realizar operaciones relacionadas con la administración de usuarios con roles de administrador en la plataforma. Desde la creación hasta la actualización y eliminación de cuentas de administradores, esta API proporciona funcionalidades completas para gestionar este tipo de usuario.
    </p>
    
    <p style="font-size: 20px; font-weight: 700;">Agricultores</p>
    <p>
        El recurso de Agricultores ofrece funcionalidades para gestionar usuarios con roles de agricultor. Desde la creación de nuevas cuentas de agricultores hasta la actualización de su información personal, esta API permite gestionar eficazmente los usuarios involucrados en la agricultura.
    </p>
    
    <p style="font-size: 20px; font-weight: 700;">Autenticación</p>
    <p>
        El recurso de Autenticación se encarga de las funcionalidades relacionadas con la autenticación de usuarios en la plataforma. Desde el inicio de sesión hasta el manejo de tokens de acceso, esta API proporciona mecanismos seguros para gestionar la autenticación de usuarios.
    </p>
    
    <p style="font-size: 20px; font-weight: 700;">Categorías</p>
    <p>
        El recurso de Categorías permite administrar las diferentes categorías de productos disponibles en la plataforma. Desde la creación de nuevas categorías hasta la eliminación de las existentes, esta API ofrece funcionalidades completas para gestionar la taxonomía de productos.
    </p>
    
    <p style="font-size: 20px; font-weight: 700;">Fotos</p>
    <p>
        El recurso de Fotos ofrece funcionalidades para gestionar las imágenes asociadas a los productos en la plataforma. Desde la carga de nuevas imágenes hasta la eliminación de las existentes, esta API permite gestionar eficazmente el contenido visual relacionado con los productos.
    </p>
    
    <p style="font-size: 20px; font-weight: 700;">Productos</p>
    <p>
        El recurso de Productos proporciona funcionalidades para administrar los productos disponibles en la plataforma. Desde la creación de nuevos productos hasta la actualización y eliminación de los existentes, esta API permite gestionar eficientemente el catálogo de productos.
    </p>
    
    <p style="font-size: 20px; font-weight: 700;">Propiedades Inmobiliarias</p>
    <p>
        El recurso de Propiedades Inmobiliarias ofrece funcionalidades para gestionar las propiedades relacionadas con la agricultura, como terrenos y edificaciones. Desde la creación de nuevas propiedades hasta la actualización y eliminación de las existentes, esta API facilita la gestión de activos inmobiliarios.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Sugerencia de Productos</p>
    <p>
        El recurso de Sugerencia de Productos permite a los usuarios sugerir nuevos productos para su inclusión en la plataforma. Desde la creación de nuevas sugerencias hasta su revisión y aprobación por parte de los administradores, esta API proporciona un mecanismo para ampliar el catálogo de productos disponibles.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Tipos de Productos</p>
    <p>
        El recurso de Tipos de Productos ofrece funcionalidades para gestionar los diferentes tipos de productos disponibles en la plataforma. Desde la creación de nuevos tipos hasta la actualización y eliminación de los existentes, esta API permite gestionar eficientemente la clasificación de productos.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Unidades de Medida</p>
    <p>
        El recurso de Unidades de Medida proporciona operaciones para gestionar las unidades de medida utilizadas en la plataforma. Desde la creación de nuevas unidades hasta la actualización y eliminación de las existentes, esta API facilita la gestión de las unidades de medida relacionadas con los productos agrícolas.
    </p>
  
    <p style="font-size: 20px; font-weight: 700;">Usuarios</p>
    <p>
        El recurso de Usuarios permite realizar operaciones CRUD sobre los usuarios registrados en la plataforma. Desde la gestión de la información personal hasta la administración de roles y permisos, esta API proporciona funcionalidades completas para gestionar los usuarios de la plataforma.
    </p>

    <p style="font-size: 20px; font-weight: 700;">Órdenes</p>
    <p>
        El recurso de Órdenes ofrece funcionalidades para gestionar las órdenes de compra realizadas en la plataforma. Desde la creación de nuevas órdenes hasta la actualización del estado de las existentes, esta API facilita la gestión de transacciones comerciales relacionadas con la agricultura.
    </p>
    
</aside>

